var jwt = require('jsonwebtoken');
var config = require('config.json');

//var validateUser = require('../routes/auth').validateUser;
module.exports = function(req, res, next) {
// When performing a cross domain request, you will recieve
// a preflighted request first. This is to check if our the app
// is safe. 
console.log('entered in validateUser');
var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'] || req.session.token;

  // decode token
  if (token) {

	jwt.verify(token, config.secret, function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } 
      else {
      	console.log('token in validateUser'+ token);
      	console.log('token verified '+decoded.exp);
      	console.log('now '+Date.now());
      	if (decoded.exp <= Date.now()) {
		/*res.status(400);
		res.json({
		"status": 400,
		"message": "Token Expired"
		});*/
		res.redirect('/login');
		return;
		}
		else
		{
			console.log(decoded);
			req.user = decoded;
			console.log('decoded username Re'+ req.user.username);
				next();
		}
	 }
	});
	} 
	else {
	/*res.status(401);
	res.json({
	"status": 401,
	"message": "Invalid Token."
	});*/
	res.redirect('/login');
	return;
	}
};
