var dbService = require('routes/services/db.service');

module.exports = function (role) {
  
   return function (req,res,next)
   {

    console.log('isAuth ' +role +' method '+req.method +' and' +req.user.username);

    dbService.hasRole(req.user.username, role)
    .then(function (correctRole) {
            console.log('then');
            console.log('correctRole :'+ correctRole);
            if(correctRole)
            next();
            else
            {
              res.status(403).send('Not Authorized to perform this action!!!');
            }
        })
        .catch(function (err) {
          console.log('catch');
          console.log('correctRole :'+ correctRole +' err '+ err);
          res.status(400).send(err);
        });
};
};