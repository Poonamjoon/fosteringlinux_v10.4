var config = require('config.json');
var express = require('express');
var router = express.Router();
var dbService = require('routes/services/db.service');
var Auth = require('../middleware/authRoles');

// routes
router.post('/authenticate', authenticateUser);
router.post('/register', registerUser);
router.post('/forgotpassword', forgotPassword);
router.get('/view', Auth(['Administrator']), getAllUser);
router.get('/current', getCurrentUser);
router.get('/name/:username', Auth(['Administrator']), getUserDeta);
router.put('/:_id', updateUser);
router.put('/user/:_id', Auth(['Administrator']), updateUserByAdmin);
router.delete('/:_id', Auth(['Administrator']), deleteUser);

module.exports = router;

function authenticateUser(req, res) {
    dbService.authenticate(req.body.username, req.body.password, req.body.role)
        .then(function (token) {
            if (token) {
                console.log('authentication successful');
                res.send({ token: token });
            } else {
                console.log('authentication failed');
                res.sendStatus(401);
            }
        })
        .catch(function (err) {
            console.log('catch in authenticateUser : user.controller.js', err);
            res.status(400).send(err);
        });
}

function registerUser(req, res) {
    dbService.create(req.body)
        .then(function () {
            console.log('then');
            res.sendStatus(200);
        })
        .catch(function (err) {
            console.log('catch');
            console.log(err);
            res.status(400).send(err);
        });
}

function forgotPassword(req, res) {
console.log('in api users.js forgotpassword function');

    dbService.usercheck(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getAllUser(req, res) {
    console.log('in api users.js getAllUser function');
    console.log('in'+req.user);
    dbService.getUsers()
        .then(function (users) {
            if (users) {
                //console.log(user);
                res.send(users);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getCurrentUser(req, res) {
    console.log('in api users.js getCurrentUser function');
    console.log('in'+req.user);
    dbService.getById(req.user.sub)
        .then(function (user) {
            if (user) {
                console.log(user);
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function getUserDeta(req, res) {
    console.log('in api users.js getUserDeta function');
    console.log('in'+req.params.username );
    dbService.getByUserName(req.params.username)
        .then(function (user) {
            if (user) {
                console.log(user);
               // res.send(user);
               res.json(user)
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}



function updateUser(req, res) {
    var userId = req.user.sub;
    if (req.params._id !== userId) {
        // can only update own account
        return res.status(401).send('You can only update your own account');
    }

    console.log(req.params);
    dbService.update(userId, req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function updateUserByAdmin(req, res) {

    dbService.updateUserDeta(req.params._id, req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}


function deleteUser(req, res) {
   /* var userId = req.user.sub;
   if (req.params._id == userId) {
        // can only delete own account
        return res.status(401).send('You cannot delete your own account');
    }*/

    dbService.delete(req.params._id )
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
} 