
angular.module('trainingApp').controller('trainerupdate.IndexController', Controller);

function Controller(TrainerService,CourseService, FlashService, $stateParams, trainerdata) {
	console.log('in trainer update indexcontroller main function');

    console.log(trainerdata);
    var tra = this;

    tra.courseList=[];

     
    tra.batchCapaOptions = [{
           name: '0',
           value: 0
        }, {
           name: '1',
           value: 1
        },{
           name: '2',
           value: 2
        }, {
           name: '3',
           value: 3
        }, {
           name: '4',
           value: 4
        }, {
           name: '5',
           value: 5
        }];
        
    //Get Course List from Course Service
    CourseService.GetAll()
            .then(function (response) {
                console.log('Success in Course GetAll');
                tra.courseList=response;
            })
            .catch(function (error) {
               FlashService.Error(error);
            });

    tra.loc=trainerdata.trgloc;
    tra.name=trainerdata.trainername;
    tra.email=trainerdata.email;
    tra.oldcoursearray=trainerdata.courses;
    tra.wkdayCapa=trainerdata.wkdaycapa;
    tra.wkendCapa=trainerdata.wkendcapa;
    tra.rem=trainerdata.remarks;
    tra.objectid=trainerdata._id;

    tra.updateTrainer =function() {
      console.log('in updatetrainer:updatetrainer function');
      TrainerService.updateTrainer ( tra.objectid,{
          trgloc:tra.loc,
          wkdaycapa:tra.wkdayCapa,
          wkendcapa:tra.wkendCapa,
          remarks:tra.rem,
          courses: tra.selectedCourse,
      })
      .then(function () {
                FlashService.Success('Trainer updated successfully!!!');
            })
      .catch(function (error) {
            console.log('Error from db ' + error);
               FlashService.Error(error);
            });   
     };    

     tra.getTrainer=function()
     {
        TrainerService.getTrainer[$stateParams.id];
     }


};