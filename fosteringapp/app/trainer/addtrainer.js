angular.module('trainingApp').controller('trainersubmit.IndexController', Controller);


function Controller($window, TrainerService, CourseService, FlashService) {
	console.log('in trainer submitindexcontroller main function');

	var tra = this;
    tra.courseList=[];

    tra.batchCapaOptions = [{
           name: '0',
           value: 0
        }, {
           name: '1',
           value: 1
        },{
           name: '2',
           value: 2
        }, {
           name: '3',
           value: 3
        }, {
           name: '4',
           value: 4
        }, {
           name: '5',
           value: 5
        }];

    //Get Course List from Course Service
    CourseService.GetAll()
            .then(function (response) {
                console.log('Success in Course GetAll');
                tra.courseList=response;
                
            })
            .catch(function (error) {
               FlashService.Error(error);
            });


    tra.submitTrainer =function() {
    console.log('trainersubmitindexcontroller:in submittrainer function');
    	TrainerService.createData ({
    		trgloc:tra.loc,
    	    trainername:tra.name,
          email:tra.email,
    	    wkdaycapa:tra.wkdayCapa,
          wkendcapa:tra.wkendCapa,
    	    remarks:tra.rem,
    	    courses: tra.selectedCourse,
    	})
        .then(function () {
                FlashService.Success('Trainer added successfully!!!');
            })
        .catch(function (error) {
            console.log('Error from db ' +error);
               FlashService.Error(error);
            });
            //ToDo : check reset part and Pristine
            tra.loc=null;
            tra.name=null;
            tra.email=null;
            tra.wkdayCapa=tra.batchCapaOptions[0].value;
            tra.wkendCapa=tra.batchCapaOptions[0].value;
            tra.rem=null;
            tra.selectedCourse=null;
            //tra.addTrainer.$setPristine(); - need to explore more
    };
    tra.cancel = function() {
        console.log('in cancel function');
        $window.location.href = "/app";
    };
};
