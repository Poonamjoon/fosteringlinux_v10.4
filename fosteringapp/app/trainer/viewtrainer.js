angular.module('trainingApp').controller('traviewall.IndexController', Controller);

function Controller($window, $state, TrainerService, FlashService, $filter, ngTableParams) {
  console.log('in trainerviewallcontroller main function');

  var tra = this;
  var tableData = [];
  tra.movetoUpdate= movetoUpdate;
  tra.showBatchList = showBatchList;

  tra.isVisible = false;
    tra.ShowHide = function () { 
      //If DIV is visible it will be hidden and vice versa.
      tra.isVisible = tra.showCol;
      console.log('in ShowHide function: showCol :' + tra.showCol + ' and isVisible :' +tra.isVisible);
    }
    
  initController();

  // Fetch data from server using RESTful API
  function initController() {
  TrainerService.getTrainerData().then(function(response) {
        console.log ('Success in getTrainerData');
        tableData = response;
        console.log (tableData);

        //Table configuration
        tra.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { trainername: "asc" },
        filter: {trgloc: '', trainername: '', email:'' }       // initial filter
        },{
        total:tableData.length,

        //Returns the data for rendering
        getData : function($defer,params){
        var filteredData = $filter('filter')(tableData, params.filter());
        console.log(filteredData);

        orderedData = $filter('orderBy')(filteredData, params.orderBy());
        console.log(orderedData);

        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        params.total(orderedData.length);
        }
        });
    })
    .catch(function (error) {
        console.log('Error in getTrainerData');
        FlashService.Error(error);
    });

  /*** Export to CSV Start ***/
  tra.helper = {
    csv: null //will be replaced by the export directive
  };

  tra.exportCsv = function($event, fileName) {
    tra.helper.csv.generate($event, "report.csv");
    location.href=tra.helper.csv.link();
  };
  /*** Export to CSV End ***/
};

//pass id here
tra.getTrainer=function(id)
 {
  console.log('in getTrainer function of controller');
    TrainerService.getTrainer(id);
 };
 function movetoUpdate(id)  {
    console.log('in movetoUpdate function');
    $window.location.href="#/trainer/" + id;
   }


tra.deleteTrainer=function(index, trainer, id, name)
{
console.log('in deleteTrainer function of controller');
 var r = confirm("Are you sure to delete trainer : " + name);
 if (r == true) {
      console.log('pressed OK to delete trainer ' + name);
              
      TrainerService.deleteTrainer(id)
          .then(function () {
              console.log('in deleteTrainer '+ index);
              //$window.location = '/login';
              var idx = tableData.indexOf(trainer);
              tableData.splice( idx, 1 );
              tra.tableParams.reload();

          })
          .catch(function (error) {
              FlashService.Error(error);
          });

  } else {
      console.log('pressed CANCEL to delete trainer ' + name);
  }
};
  

 function showBatchList(emailId)
 {
      console.log('in showBatchList function for Trainer Email'+ emailId);
      $state.go("viewSpecificBatch", {object:emailId});
 } 

}
