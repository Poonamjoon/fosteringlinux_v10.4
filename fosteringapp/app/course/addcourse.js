(function () {
'use strict';

angular.module('trainingApp')
       .controller('addcourse.IndexController', Controller);
       
function Controller($window, $location, CourseService, FlashService) {

        var cou = this;
        cou.isTotalCalculated = true;
        cou.totalfee = 0;

        cou.updateTotal = updateTotal;
        cou.cancel = cancel
        cou.addNewCourse = addNewCourse;

        //Set default value of Service Tax.
        cou.srvctax = 14.5;
        //cou.isCollapsed = true;
        cou.regFee = 10;
        cou.firInsFee = 90;
        cou.secInsFee = 0;

        cou.statusOptions = [{
           name: 'Active',
           value: 'Active'
        }, {
           name: 'Obsolete',
           value: 'Obsolete'
        }];

        function updateTotal(){
        if (!cou.isTotalCalculated) return;
        cou.totalfee = Math.round(cou.fee + ((cou.fee * cou.srvctax)/100));
            console.log('Total fee :'+cou.totalfee);
        }

        function addNewCourse() {
            console.log('in addNewCourse function')
            if((cou.regFee + cou.firInsFee + cou.secInsFee) > 100)
            {
                $window.alert('Fee breakup cannot exceed 100%. Please check again.')
            }
            else if((cou.regFee + cou.firInsFee + cou.secInsFee) < 100)
            {
                $window.alert('Fee breakup cannot less than 100%. Please check again.')
            }
            else
            {
            	CourseService.Add({
            		id:cou.id,
            		name:cou.name,
            		fee:cou.fee,
                    regfee:cou.regFee,
                    firinsfee:cou.firInsFee,
                    secinsfee:cou.secInsFee,
                    srvctax:cou.srvctax,
                    totalfee:cou.totalfee,
                    duration:cou.duration,
            		remarks:cou.remarks,
                    status:cou.status
            	})
            	.then(function () {
                    FlashService.Success('Course added successfully!!!');
                    cou.id=null;
                    cou.name=null;
                    cou.fee=null;
                    cou.regFee = 10;
                    cou.firInsFee = 90;
                    cou.secInsFee = 0;
                    cou.totalfee=null;
                    cou.duration=null;
                    cou.remarks=null;
                    cou.status=cou.statusOptions[0].value;
                    cou.srvctax=14.5;
                })
                .catch(function (error) {
                   FlashService.Error(error);
                });
                //ToDo : check reset part and Pristine
                
                //cou.addCourse.$setPristine();
            }
        }
    
        function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
        }
    }
})();
