(function () {
'use strict';

angular.module('trainingApp')
       .controller('viewcourse.IndexController', ViewController)

function ViewController($window, CourseService, FlashService, $filter, ngTableParams) {
    var cou = this;
    var response=[];
    var tableData = [] ;
    cou.movetoUpdate = movetoUpdate;
    cou.cancel = cancel;
    
    console.log('in ViewController function');

    cou.isVisible = false;
    cou.ShowHide = function () { 
      //If DIV is visible it will be hidden and vice versa.
      cou.isVisible = cou.showCol;
      console.log('in ShowHide function: showCol :' + cou.showCol + ' and isVisible :' +cou.isVisible);
    }

    initController();

    // Fetch data from server using RESTful API
    function initController() {
    CourseService.GetAll().then(function(response) {
        tableData = response;
        console.log (tableData);

        //Table configuration
        cou.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { name: "asc" },
        filter: {name: '' }       // initial filter
        },{
            total:tableData.length,
            //Returns the data for rendering

            getData : function($defer,params){
            var filteredData = $filter('filter')(tableData, params.filter());
            console.log(filteredData);

            var orderedData = $filter('orderBy')(filteredData, params.orderBy());
            console.log(orderedData);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            params.total(orderedData.length);
            }
        });
    })
    .catch(function (error) {
        console.log('Error in GetAll from CourseService');
        FlashService.Error(error);
    });
    };

    function movetoUpdate(id)  {
    console.log('in movetoUpdate function');
    $window.location.href="#/courses/" + id;
   } 

   function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
    }

};

})();
