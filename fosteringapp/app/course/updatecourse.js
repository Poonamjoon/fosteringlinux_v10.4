
angular.module('trainingApp').controller('updatecourse.IndexController', Controller);

function Controller($window, CourseService, FlashService, $stateParams, coursedata) {
	console.log('in course update indexcontroller main function');

    console.log(coursedata);
    var cou = this;
    cou.isTotalCalculated = true;
    cou.updateTotal = updateTotal;

    cou.id=coursedata.id;
    cou.name=coursedata.name;
    cou.fee=coursedata.fee;
    cou.regFee = coursedata.regfee;
    cou.firInsFee = coursedata.firinsfee;
    cou.secInsFee = coursedata.secinsfee;
    cou.srvctax = coursedata.srvctax;
    cou.totalfee=coursedata.totalfee;
    cou.duration=coursedata.duration;
    cou.remarks=coursedata.remarks;
    cou.status=coursedata.status;
    cou.objectid=coursedata._id;
     

    cou.statusOptions = [{
           name: 'Active',
           value: 'Active'
        }, {
           name: 'Obsolete',
           value: 'Obsolete'
        }];

    
    function updateTotal(){
        if (!cou.isTotalCalculated) return;
        cou.totalfee = Math.round(cou.fee + ((cou.fee * cou.srvctax)/100));
            console.log('Total fee :'+cou.totalfee);
        }

    cou.updateCourse =function() {
      console.log('in updatecourse:updateCourse function');
      console.log('Tax :' +cou.srvctax + 'Total ::' +cou.totalfee + 'Dua ::' +cou.duration);
            if((cou.regFee + cou.firInsFee + cou.secInsFee) > 100)
            {
                $window.alert('Fee breakup cannot exceed 100%. Please check again.')
            }
            else if((cou.regFee + cou.firInsFee + cou.secInsFee) < 100)
            {
                $window.alert('Fee breakup cannot less than 100%. Please check again.')
            }
            else
            {
                CourseService.Update ( cou.objectid,{
                    id:cou.id,
                    name:cou.name,
                    fee:cou.fee,
                    regfee:cou.regFee,
                    firinsfee:cou.firInsFee,
                    secinsfee:cou.secInsFee,
                    srvctax:cou.srvctax,
                    totalfee:cou.totalfee,
                    duration:cou.duration,
                    remarks:cou.remarks,
                    status:cou.status
                })
                .then(function () {
                          FlashService.Success('Course details have been successfully updated!!!');
                      })
                .catch(function (error) {
                      console.log('Error from db ::' + error);

               //       console.log(error.code);
              if (error.code == 11000)
              {                    
                  var field = error.message.split('.$')[1];
                  field = field.split(' dup key')[0];
                  field = field.substring(0, field.lastIndexOf('_'));

                  console.log('field : ' + field);
                  var msg;
                  if(field=="id")
                    msg="Failed to Update Course. Duplicate Course ID found !";
                  else
                    msg="Failed to Update Course. Duplicate Course Name found !";

                  FlashService.Error(msg);   
                  
              }
              else
              {
                FlashService.Error(error);
              }
             
              });

            }
     };    

     cou.getCourse=function()
     {
        console.log('in getCourse func')
        return CourseService.Get[$stateParams.id];
     };

};