(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('clientmail', Service);


    function Service($http, $q) {

        console.log('in App : clientmail');

        var service = {};

        service.submitinquirym = submitinquirym;
        service.createmail =createmail;
        service.getmailinfo = getmailinfo;

        return service;

        function createmail(emailModel) {

            console.log('in mail service : createData function');
            return $http.post('/app/createmail',emailModel).then(handleSuccess, handleError);

        }

        function submitinquirym(mailbody) {
            console.log(mailbody);
            console.log('in submitinquirym : Add function');
            return $http.post('/app/submitinquirym', mailbody).then(handleSuccess, handleError);
        }

       function getmailinfo(){

           console.log('in clientmail sevice : getData function');
           return $http.get('/app/mailinfo').then(handleSuccess, handleError);
       }

        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 