(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('CourseService', Service);


    function Service($http, $q) {

        console.log('in App : CourseService');

        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByName = GetByName;
        service.Add = Add;
        service.Update = Update;
        service.Delete = Delete;  

        return service;

        function GetAll() {
            console.log('in CourseService : GetAll function');
            return $http.get('/app/course').then(handleSuccess, handleError);

        }

        function GetById(_id) {
            console.log('in CourseService : Get function for ' + _id);
            return $http.get('/app/course/' + _id).then(handleSuccess, handleError);
        }

        function GetByName(couName) {
            console.log('in CourseService : Get function for ' + couName);
            return $http.get('/app/course/', {params: {name: couName}}).then(handleSuccess, handleError);
        }

        function Add(CourseModel) {
            console.log('in CourseService : Add function');
            return $http.post('/app/course', CourseModel).then(handleSuccess, handleError);
        }

        function Update(_id, course) {
            console.log('in CourseService : Update function');
            return $http.put('/app/course/' +_id, course).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            console.log('in CourseService : Delete function');
            return $http.delete('/app/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 