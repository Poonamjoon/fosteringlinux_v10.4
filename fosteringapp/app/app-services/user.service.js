(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('UserService', Service);


    function Service($http, $q) {

        console.log('in UserService');

        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Register = Register;
        service.Update = Update;
        service.UpdateByAdmin = UpdateByAdmin;
        service.Delete = Delete;

        return service;

        function GetCurrent() {
            
            return $http.get('/api/users/current').then(handleSuccess, handleError);
        }

        function GetAll() {
            return $http.get('/api/users/view').then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get('/api/users/' + _id).then(handleSuccess, handleError);
        }

        function GetByUsername(username) {
            return $http.get('/api/users/name/' + username).then(handleSuccess, handleError);
        }

        function Create(user) {
            return $http.post('/api/users', user).then(handleSuccess, handleError);
        }

        function Register(user) {
            console.log('in Register user service function');
            return $http.post('/api/users/register', user).then(handleSuccess, handleError);
        }
        function Update(user) {
            return $http.put('/api/users/' + user._id, user).then(handleSuccess, handleError);
        }
        function UpdateByAdmin(_id, user) {
            return $http.put('/api/users/user/' + _id, user).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete('/api/users/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 