(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('BatchService', Service);


    function Service($http, $q) {

        console.log('in App : BatchService');

        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Add = Add;
        service.GetTrainer = GetTrainer;
        service.Update = Update;
        service.Delete = Delete;  
        service.GetByBatchId = GetByBatchId; //tejas

        return service;

        /*function GetAll(fltrId, fltrVal) {
            console.log('in BatchService : GetAll function :: '+ fltrId + fltrVal);
            return $http.get('/app/batch', {params: {id: fltrId, val: fltrVal}}).then(handleSuccess, handleError);

        }*/
        function GetAll(email) {
            console.log('in BatchService : GetAll function ');
            return $http.get('/app/batch', {params: {traEmail:email}}).then(handleSuccess, handleError);

        }

        function GetById(_id) {
            console.log('in BatchService : Get function for ' + _id);
            return $http.get('/app/batch/' + _id).then(handleSuccess, handleError);
        }

        function GetByBatchId(id) {
            console.log('in BatchService : Get function for ' + id);
            return $http.get('/app/batch/trainer' ,{params: {id: id}}).then(handleSuccess, handleError);
        }

        function Add(batchModel) {
            console.log('in BatchService : Add function');
            return $http.post('/app/batch', batchModel).then(handleSuccess, handleError);
        }

        function GetTrainer(course, location, dayType) { 
            console.log('in BatchService : GetTrainer function '+ course + location);
            return $http.get('/app/batch/findtrainer/', {params: {course: course, loc: location, day: dayType}}).then(handleSuccess, handleError);
        }

        function Update(_id, batch) {
            console.log('in BatchService : Update function');
            return $http.put('/app/batch/' +_id, batch).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            console.log('in BatchService : Delete function');
            return $http.delete('/app/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 