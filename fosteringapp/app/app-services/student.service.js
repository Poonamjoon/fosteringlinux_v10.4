(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('StudentService', Service);


    function Service($http, $q) {

        console.log('in App : StudentService');

        var service = {};

        service.GetAll = GetAll;
        service.FindBatch = FindBatch;
        service.GetByEmail = GetByEmail;
        service.GetStuFeedBDet = GetStuFeedBDet;
        service.Add = Add;
        service.Update = Update;
        service.Delete = Delete;  
        service.GetById = GetById; 
        service.CreateFeedbackData = CreateFeedbackData; 

        return service;

        function GetAll(batchId) {
            console.log('in StudentService : GetAll function for batch id '+ batchId);
            return $http.get('/app/student', {params: {batId:batchId}}).then(handleSuccess, handleError);

        }

        function FindBatch(loc, course) {
            console.log('in StudentService : FindBatch function for ' + loc + course);
            return $http.get('/app/student/findbatch', {params: {loc:loc, course:course}}).then(handleSuccess, handleError);
        }

        function GetByEmail(email) {
            console.log('in StudentService : Get function for ' + email);
            return $http.get('/app/student/', {params: {email:email}}).then(handleSuccess, handleError);
        }
	
        function GetStuFeedBDet(email) {
            console.log('in StudentService : Get function for ' + email);
            return $http.get('/app/student/feedback/', {params: {email:email}}).then(handleSuccess, handleError);
        }

	function GetById(_id) {
            console.log('in StudentService : Get function for ' + _id);
            return $http.get('/app/studentid/'+ _id).then(handleSuccess, handleError);
        }
	
        function GetByBatId(batId) {
            console.log('in StudentService : Get function for ' + batId);
            return $http.get('/app/student/', {params: {id:batId}}).then(handleSuccess, handleError);
        }

        function Add(studentModel) {
            console.log('in StudentService : Add function');
            return $http.post('/app/student', studentModel).then(handleSuccess, handleError);
        }

        function Update(_id, student) {
            console.log('in StudentService : Update function');
            return $http.put('/app/student/' +_id, student).then(handleSuccess, handleError);
        }

        function Delete(_id) {
            console.log('in StudentService : Delete function');
            return $http.delete('/app/' + _id).then(handleSuccess, handleError);
        }

        function CreateFeedbackData  (StudentFeedbackModel) {
         
                console.log('in inquiryservice : createData function');
                return $http.post('/app/studentfeedback',StudentFeedbackModel).then(handleSuccess, handleError);
       
        }
        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 