(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('TrainerService', Service);


    function Service($http, $q) {

        console.log('in App : TrainerService');

        var service = {};

        service.getTrainerData = getTrainerData;
        service.getTrainer = getTrainer;
        service.getTrainerName = getTrainerName; 
        service.createData = createData;
        service.updateTrainer = updateTrainer;
        service.deleteTrainer = deleteTrainer;  

        return service;

        function getTrainerData() {
            console.log('in TrainerService : getTrainerData function');
            return $http.get('/app/trainers').then(handleSuccess, handleError);

        }

        function getTrainer(id) {
            console.log('in TrainerService : getTrainer function');
            return $http.get('/app/trainers/' + id).then(handleSuccess, handleError);

        }

        function getTrainerName(tmail) {
            console.log('in TrainerService : getTrainerName function'+tmail);
            return $http.get('/app/trainersname/' + tmail).then(handleSuccess, handleError);
        }
        
        function createData(TrainerModel) {
            console.log('in TrainerService : createData function');
            return $http.post('/app/trainers',TrainerModel).then(handleSuccess, handleError);
        }

        function updateTrainer(id,TrainerModel) {
            console.log('in TrainerService : updateTrainer function');
            return $http.put('/app/trainers/' + id,TrainerModel).then(handleSuccess, handleError);
        }

        function deleteTrainer(_id) {
            console.log('in TrainerService : deleteTrainer function');
            return $http.delete('/app/trainers/' + _id).then(handleSuccess, handleError);
        }

        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }

})();
 