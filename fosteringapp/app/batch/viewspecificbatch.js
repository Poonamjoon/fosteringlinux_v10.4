(function () {
'use strict';


angular.module('trainingApp')
       .controller('viewspecificbatch.IndexController', ViewController);


function ViewController($window, BatchService, FlashService, $state, $filter, ngTableParams) {
    var bat = this;
    //bat.fltrId = $state.params.object.filterId;
    //bat.fltrVal = $state.params.object.filterVal;
    var response=[];
    var tableData = [] ;
    bat.moveToUpdateBatch = moveToUpdateBatch;
    bat.deleteBatch = deleteBatch;
    bat.showStudentList = showStudentList;
    var fltrTraEmailId = null;
    
    console.log('($state.params.object '+$state.params.object);
    if($state.params.object !=null)
    {
        fltrTraEmailId = $state.params.object;
        $state.params.object = null;
    }

    console.log('in ViewController function with Id and Val as '+bat.fltrId+bat.fltrVal);

    bat.isVisible = false;
    bat.ShowHide = function () { 
      //If DIV is visible it will be hidden and vice versa.
      bat.isVisible = bat.showCol;
      console.log('in ShowHide function: showCol :' + bat.showCol + ' and isVisible :' +bat.isVisible);
  }

    initController();

    // Fetch data from server using RESTful API
    function initController() {
    //BatchService.GetAll(bat.fltrId, bat.fltrVal).then(function(response) {
      BatchService.GetAll(fltrTraEmailId).then(function(response) {
        console.log('in BatchService Get All function');
        tableData = response;
        console.log (tableData);

        //Table configuration
        bat.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { name: "asc" },
        filter: {trainer:"", targetstren:"" }       // initial filter
        },{
            total:tableData.length,
            //Returns the data for rendering

            getData : function($defer,params){
            var filteredData = $filter('filter')(tableData, params.filter());
            console.log(filteredData);

            var orderedData = $filter('orderBy')(filteredData, params.orderBy());
            console.log(orderedData);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            params.total(orderedData.length);
            }
        });
    })
    .catch(function (error) {
        console.log('Error in GetAll from CourseService');
        FlashService.Error(error);
    });

    };

    function moveToUpdateBatch(batObj)
    {
        console.log('in moveToUpdateBatch function for Obj'+ batObj);
        $state.go("updateBatch", {object:batObj});
 
    }

    function showStudentList(batId)
    {       
        console.log('in showStudentList function for batch id'+ batId);
        $state.go("viewAllStudent", {object:batId});
 
    }

    function deleteBatch(index, batch, id)
    {
    console.log('in deleteTrainer function of controller');
     /*var r = confirm("Are you sure to delete trainer : " + name);
     if (r == true) {
          console.log('pressed OK to delete trainer ' + name);
                  
          TrainerService.deleteTrainer(id)
              .then(function () {
                  console.log('in deleteTrainer '+ index);
                  //$window.location = '/login';
                  var idx = tableData.indexOf(trainer);
                  tableData.splice( idx, 1 );
                  tra.tableParams.reload();

              })
              .catch(function (error) {
                  FlashService.Error(error);
              });


      } else {
          console.log('pressed CANCEL to delete trainer ' + name);
      }*/
    };

}

})();
