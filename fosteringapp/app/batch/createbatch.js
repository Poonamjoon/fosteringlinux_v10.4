
(function () {
'use strict';

angular.module('trainingApp')
       .controller('createbatch.IndexController', Controller);

       
function Controller($window, $scope, $filter, CourseService, BatchService, FlashService) {
        var bat = this;
        bat.courseList=[];
        bat.getTrainer = getTrainer;
        bat.dayDiff = dayDiff;
        bat.createNewBatch = createNewBatch;
        bat.findBatchId = findBatchId;
        bat.cancel = cancel;
        bat.findCouDeta = findCouDeta;
        bat.resetTrainerData = resetTrainerData;
        bat.isGetTrainer = false;
        bat.trainername = null;
        bat.trainer = null;
        bat.findTraData = findTraData;

        bat.displayTime = new Date();
        bat.displayTime2 = new Date();

        bat.ismeridian = false;
        bat.meridian = ['AM', 'PM'];
        
        bat.startTime = 'n/a';
        bat.endTime = 'n/a';
        var batId = null;

        bat.statusOptions = [{
           name: 'Scheduled',
           value: 'Scheduled'
        }, {
           name: 'Cancelled',
           value: 'Cancelled'
        }, {
           name: 'Completed',
           value: 'Completed'
        }];

        bat.trainerListOptions = [{
           name: 'Select Trainer',
           value: null
        }];

        bat.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };

        bat.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        bat.format = bat.formats[2];
        bat.altInputFormats = ['M!/d!/yyyy'];


        //Get Course List from Course Service
        CourseService.GetAll()
        .then(function (response) {
            console.log('Success in Course GetAll');
            bat.courseList=response;
            
        })
        .catch(function (error) {
           console.log('Error in Course GetAll');
           FlashService.Error(error);
        });       

        bat.open1 = function() {
          bat.popup1.opened = true;
        };

        bat.popup1 = {
          opened: false
        };  

        bat.open2 = function() {
          bat.popup2.opened = true;
        };


        bat.popup2 = {
          opened: false
        };  
 
       
        $scope.$watch('bat.displayTime', function(newValue, oldValue) {
          var hour = bat.displayTime.getHours()-(bat.displayTime.getHours() >= 12 ? 12 : 0),
              hour = hour<10 ? '0'+hour : hour,
              minutes = (bat.displayTime.getMinutes()<10 ? '0' :'') + bat.displayTime.getMinutes(), 
              period = bat.displayTime.getHours() >= 12 ? 'PM' : 'AM';
          bat.startTime = hour+':'+minutes+' '+period
        });

        $scope.$watch('bat.displayTime2', function(newValue, oldValue) {
          var hour = bat.displayTime2.getHours()-(bat.displayTime2.getHours() >= 12 ? 12 : 0),
              hour = hour<10 ? '0'+hour : hour,
              minutes = (bat.displayTime2.getMinutes()<10 ? '0' :'') + bat.displayTime2.getMinutes(), 
              period = bat.displayTime2.getHours() >= 12 ? 'PM' : 'AM';
          bat.endTime = hour+':'+minutes+' '+period
        });
         
        function resetTrainerData()
        {
           console.log('in resetTrainerData function');
           bat.trainer = null;
           bat.trainername = null;
           bat.trainerList = '';
           bat.isGetTrainer = false;

        }

        function getTrainer() {
        var startDate = new Date(bat.plStartDate);
        var wkday = startDate.getDay();

        BatchService.GetTrainer(bat.course, bat.loc, wkday)
            .then(function (response) {
                console.log('Success in getTrainer ' + response);
                bat.trainerList=response;
                
                if (response.length == 0)
                {
                    console.log('Trainer not found');
                    var day = null;
                    if ((wkday >0) && (wkday <5))
                    {
                      day="Weekday"
                    }
                    else
                    {
                     day="Weekend" 
                    }
                    var filtDate = $filter('date')(startDate, 'dd.MM.yyyy');
                    $window.alert('No Trainer available for Starting Date "'+filtDate +'" Day "'+day+'" Course "'+ bat.course +'" and Location "'+ bat.loc +'".');
                }
                else
                {
                  bat.isGetTrainer = true;
                }    
            })
            .catch(function (error) {
               console.log('Error in getTrainer');
               if(error.message)
               {
                 FlashService.Error(error.message);
               }
               else
               {
                FlashService.Error(error);
               }
            });
            
        };
        

        function findBatchId(start)
        {
            var locId = null;
            var courseId = bat.course;
            var month = null;
            if (bat.loc=="Gurgaon")
                locId = "GGN";
            else if (bat.loc=="Noida")
                locId ="NOI";
            month = start.getMonth();
           //var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
           //var mon = months[month];
           var year = (start.getFullYear() - 2000);

            if (month<9)
            {
                batId = locId + '/' + courseId + '/0' + (month +1) + year;
            }
            else
            {
              batId = locId + '/' + courseId + '/' + (month +1) + year; 
            }
            console.log('batch Id ::' + batId);


        }

        function dayDiff ()
        {
            var endDate = bat.plEndDate;
            console.log(endDate);
            var mdy = endDate.split('.');
            var date2 = new Date(mdy[2], mdy[0]-1, mdy[1]);
            console.log(mdy[2]+mdy[0]+mdy[1]);
            //var date2 = bat.formatString(bat.plEndDate);

            /*var date1 = bat.formatString(bat.plStartDate);
            console.log(date2 +' '+date1);
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());   
            bat.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
            console.log('dayDifference :: '+bat.dayDifference);*/
        }

        function findCouDeta () 
        {
          console.log('in findCouDeta ::' + bat.couObjIdx);
          resetTrainerData();
          if(bat.couObjIdx)
          {
          bat.course = bat.courseList[bat.couObjIdx].name;
          bat.plDuration = bat.courseList[bat.couObjIdx].duration;
          console.log('selected course ::'+ bat.course +' and duration ::'+bat.plDuration);
          //bat.isShowDura = true;
          }
          else
          {
            bat.plDuration =null;
          }

        }

        function findTraData () {
               var idx = bat.traObjIdx;
               console.log('in findTraData  function for index '+idx);
               if(idx)
               {
                 bat.trainername = bat.trainerList[idx].trainername;
                  bat.trainer = bat.trainerList[idx].email;               
               }
               console.log('Trainer Name is'+bat.trainername + ' and email '+bat.trainer);
        }

        function createNewBatch()
        {
            console.log('in createNewBatch function');

            console.log("Dates :" + bat.plStartDate + " " + bat.plEndDate);
            
            var startDate = new Date(bat.plStartDate);
            var endDate = new Date(bat.plEndDate);

            if (bat.trainer==null)
            {
              $window.alert("Select suitable Trainer for the Batch");
              return;
            }
            findBatchId(startDate);

            BatchService.Add({
                loc:bat.loc,
                course:bat.course,
                trainer:bat.trainer,
                trainername:bat.trainername,
                //trainerobj: bat.trainerObj,
                starttimeslot:bat.startTime,
                endtimeslot:bat.endTime,
                plstartdate:startDate,
                plenddate:endDate,
                plduration:bat.plDuration,
                id:batId,
                targetstren:bat.tarStrength,
                actualstren:0,
                status:bat.status,
                remarks:bat.remarks
            })
            .then(function () {
                FlashService.Success('Batch added successfully!!!');

                $window.alert("Batch created successfully. Assign eligible Students to New Batch " +batId +".");
                bat.loc =null;
                bat.course=null;
                bat.couObjIdx = null;
                bat.trainerList = null;//bat.trainerList[0].value;
                bat.trainer=null;
                bat.trainername=null;
                //bat.startTime={{new Date() | date:shortTime}};
                //bat.endTime=new Date() | date:shortTime}};
                bat.plStartDate=null;
                bat.plEndDate=null;
                bat.plDuration=null;
                bat.batId=null;
                bat.tarStrength=null;
                bat.status=bat.statusOptions[0].value;
                bat.remarks=null;
                bat.isGetTrainer = false;

                
            })
            .catch(function (error) {
              console.log(error);
               FlashService.Error(error);
            });
            //ToDo : check reset part and Pristine
            
            
        };
    
        function cancel() 
        {
            console.log('in cancel function');
            $window.location.href = "/app";
        };
  

    }
})();
