
(function () {
'use strict';

angular.module('trainingApp')
       .controller('updatebatch.IndexController', Controller);

       
function Controller($window, $state, $filter, CourseService, BatchService, FlashService) {
        var bat = this;
        
        bat.updaObjId = $state.params.object._id;
        console.log('in updatebatch controller for '+bat.updaObjId);
        bat.batId = $state.params.object.id;
        bat.loc = $state.params.object.loc;
        bat.course = $state.params.object.course;
        bat.trainer = $state.params.object.trainer;

        bat.batStartTime = $state.params.object.starttimeslot;
        bat.batEndTime = $state.params.object.endtimeslot;
        bat.plStartDate = $filter('date')(new Date($state.params.object.plstartdate), 'dd.MM.yyyy');
        bat.plEndDate = $filter('date')(new Date($state.params.object.plenddate), 'dd.MM.yyyy');
        bat.plDuration = $state.params.object.plduration;
        //console.log('act start date '+$state.params.object.actstartdate);
        if($state.params.object.actstartdate)
        {
           bat.actStartDate = new Date($state.params.object.actstartdate);
        }
        else
        {
           bat.actStartDate = null; 
        }
        if($state.params.object.actenddate)
        {
           bat.actEndDate = new Date($state.params.object.actenddate);
        }
        else
        {
          bat.actEndDate = null; 
        }
        bat.tarStrength = $state.params.object.targetstren;
        bat.actStrength = $state.params.object.actualstren;
        bat.status = $state.params.object.status;
        bat.remarks = $state.params.object.remarks;

        bat.getTrainer = getTrainer;
        bat.updateBatch = updateBatch;

        bat.newTrainer = bat.trainer;
        bat.isShowNewTrainer = false;
        bat.newTraName = null;
        bat.newTraEmail = null;
        bat.findTraData = findTraData;

        bat.startTime = new Date();

        bat.ismeridian = false;
        bat.meridian = ['AM', 'PM'];

          bat.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
          };


          bat.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          bat.format = bat.formats[2];
          bat.altInputFormats = ['M!/d!/yyyy'];

          bat.open3 = function() {
            bat.popup3.opened = true;
          };


          bat.popup3 = {
            opened: false
          };  

          bat.open4 = function() {
            bat.popup4.opened = true;
          };


          bat.popup4 = {
            opened: false
          };  
        
        bat.statusOptions = [{
           name: 'Scheduled',
           value: 'Scheduled'
        }, {
           name: 'Cancelled',
           value: 'Cancelled'
        }, {
           name: 'Completed',
           value: 'Completed'
        }];

        bat.isStatusComp = (bat.status==bat.statusOptions[2].value);
        bat.prevStatus = bat.status;

        function getTrainer() {
        BatchService.GetTrainer(bat.course, bat.loc)
            .then(function (response) {
                console.log('Success in getTrainer ' + response);
                bat.trainerList=response;
                

                  if (response.length == 0)
                  {
                      console.log('Trainer not found');
                      var startDate = new Date(bat.plStartDate);
                      var wkday = startDate.getDay();
                      var day = null;
                      if ((wkday >0) && (wkday <5))
                      {
                        day="Weekday"
                      }
                      else
                      {
                       day="Weekend" 
                      }
                      $window.alert('No Trainer available for Day "'+day+'" Course "'+ bat.course +'" and Location "'+ bat.loc +'".');
                  }
                  else
                  {
                      bat.isShowNewTrainer = true;
                  }    
                })
                .catch(function (error) {
                   console.log('Error in getTrainer');
                   if(error.message)
                   {
                     FlashService.Error(error.message);
                   }
                   else
                   {
                    FlashService.Error(error);
                   }
            });            
        };
        
        function findTraData () {
               var idx = bat.traObjIdx;
               console.log('in findTraData  function for index '+idx);
               if(idx)
               {
                 bat.newTraName = bat.trainerList[idx].trainername;
                 bat.newTraEmail = bat.trainerList[idx].email;               
               }
               console.log('Trainer Name is'+bat.newTraName + ' and email '+bat.newTraEmail);
        }

        function updateBatch(){

            console.log('in updateBatch function');

            console.log("Dates :" + bat.actStartDate + " " + bat.actEndDate);
            if (bat.tarStrength < bat.actStrength)
            {
                $window.alert('Batch Capacity is incorrect. Batch Capacity cannot be less than Batch Current Strength');  
            } 
            else if ((bat.status ==bat.statusOptions[1].value) && (bat.actStrength >0))
            {
                $window.alert('Batch Status cannot be changed to Cancelled beacuse Students are assigned to this Batch. First reassign Batch for Students.');
            }
            else
            {            
            var startDate = new Date(bat.actStartDate);
            var endDate = new Date(bat.actEndDate);
            //Find Schedule Variance if Batch is completed.
            var schVar = 0;
            var r = false;
            if((bat.prevStatus!=bat.statusOptions[2].value) && (bat.status==bat.statusOptions[2].value))
            {              
              r = confirm("Are you sure to mark the Batch Status as Completed? Once form updated successfully, you will not able to change any details except Remarks.");
              if (r == false) {
                  return;
              }
              else
              {
                //find schedule variance
              }
            }
            
            var traName = bat.trainername;
            var traMail = bat.trainer;

            if (bat.newTraEmail && (bat.newTraMail != bat.trainer))
            {
              traMail = bat.newTraEmail;
              traName = bat.newTraName;
            }


            BatchService.Update(bat.updaObjId, {
                id:bat.batId,
                trainer:traMail,
                trainername:traName,
                actstartdate:startDate,
                actenddate:endDate,
                targetstren:bat.tarStrength,
                status:bat.status,
                remarks:bat.remarks
            })
            .then(function () {
                FlashService.Success('Batch '+bat.batId+' updated successfully!!!');

                if((bat.prevStatus!=bat.statusOptions[2].value) && (bat.status==bat.statusOptions[2].value))
                {
                  bat.isStatusComp = true;
                  bat.prevStatus = bat.status;
                  $window.alert("Training batch is over. Kindly issue Certificates and collect Feedback from Students.");
                }
            })
            .catch(function (error) {
               console.log(error);
               FlashService.Error(error);
            });
          }
        };
    
        
    }
})();
