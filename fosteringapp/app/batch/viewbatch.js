(function () {
'use strict';

angular.module('trainingApp')
       .controller('viewbatch.IndexController', ViewController);


function ViewController($window, $state, CourseService, FlashService) {
    var bat = this;
    bat.showCourse = false;
    bat.viewBatCou = viewBatCou;
    bat.viewBatLoc = viewBatLoc;
    bat.showFilter = showFilter;
    bat.cancel = cancel;

    function showFilter(input)
    {
        if (input=='course')
        {
            bat.showCourse=true;
            bat.showLoc=false; 
            bat.showDate=false;           
        }
        else if(input=="location")
        {
            bat.showCourse=false;
            bat.showLoc=true;              
            bat.showDate=false;           
        }
        else
        {
            bat.showCourse=false;
            bat.showLoc=false;              
            bat.showDate=true;              
        }
    }

    bat.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            //maxDate: new Date(2020, 5, 22),
            //minDate: new Date(),
            startingDay: 1
          };


          bat.open1 = function() {
            bat.popup1.opened = true;
          };

          bat.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          bat.format = bat.formats[0];
          bat.altInputFormats = ['M!/d!/yyyy'];

          bat.popup1 = {
            opened: false
          };  

        bat.open2 = function() {
            bat.popup2.opened = true;
          };


          bat.popup2 = {
            opened: false
          }
    //Get Course List from Course Service
        CourseService.GetAll()
            .then(function (response) {
                console.log('Success in Course GetAll');
                bat.courseList=response;
                
            })
            .catch(function (error) {
               console.log('Error in getTrainer');
               FlashService.Error(error);
            });

    function viewBatCou(seleCou) {
        console.log('in viewBatCou function for Course :'+seleCou);
        var obj = {filterId: 'Course', filterVal: seleCou};
        $state.go("viewSpecificBatch", {object:obj});
    }

    function viewBatLoc(seleLoc) {
        console.log('in viewBatLoc function for Location :'+seleLoc);
        var obj = {filterId: 'Location', filterVal: seleLoc};
        $state.go("viewSpecificBatch", {object:obj});
    }
    
    function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
        };

}

})();
