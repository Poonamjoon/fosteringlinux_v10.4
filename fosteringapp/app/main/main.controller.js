﻿
(function () {
'use strict';

angular.module('trainingApp')
       .controller('main.IndexController', Controller);
       
function Controller(UserService, $rootScope) {
            $rootScope.user = null;

       initController();

        function initController() {
console.log('in home index controller initcontroller function');
            // get current user
            UserService.GetCurrent().then(function (user) {
                console.log(user);
                $rootScope.user = user;
                console.log($rootScope.user);

                //Populate roleidx based on Role String
                if(user.role=='Administrator') 
                $rootScope.user.roleIdx = 0;
                else if(user.role=='Counsellor') 
                $rootScope.user.roleIdx = 1;
                else if(user.role=='Guest') 
                $rootScope.user.roleIdx = 2;
                else if(user.role=='Student') 
                $rootScope.user.roleIdx = 3;
                else if(user.role=='Trainer') 
                $rootScope.user.roleIdx = 4;
                else
                $rootScope.user.roleIdx = 5;
            });
        
        }

    }
})();
