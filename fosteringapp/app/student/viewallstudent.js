(function () {
'use strict';

angular.module('trainingApp')
       .controller('viewallstudent.IndexController', ViewController);


function ViewController($window, $state, StudentService, FlashService, $filter, ngTableParams) {
    var stu = this;
    var response=[];
    var tableData = [] ;
    var fltrBatId = null;

    console.log('($state.params.object '+$state.params.object);
    if($state.params.object !=null)
    {
        fltrBatId = $state.params.object;
        $state.params.object = null;
    }

    console.log('in viewallstudent.js:ViewController function');
    console.log('Batch Id : '+stu.fltrBatId);

    stu.isVisible = false;
    stu.ShowHide = function () { 
              //If DIV is visible it will be hidden and vice versa.
              stu.isVisible = stu.showCol;
              console.log('in ShowHide function: showCol :' + stu.showCol + 'and isVisible :' +stu.isVisible);
          }


    initController();

    // Fetch data from server using RESTful API
    function initController() {
    StudentService.GetAll(fltrBatId).then(function(response) {
        tableData = response;
        console.log (tableData);

        //Table configuration
        stu.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { name: "asc" },
        filter: {name: '' }       // initial filter
        },{
            total:tableData.length,
            //Returns the data for rendering

            getData : function($defer,params){
            var filteredData = $filter('filter')(tableData, params.filter());
            console.log(filteredData);

            var orderedData = $filter('orderBy')(filteredData, params.orderBy());
            console.log(orderedData);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            params.total(orderedData.length);
            }
        });
    })
    .catch(function (error) {
        console.log('Error in GetAll from StudentService');
        FlashService.Error(error);
    });

    };

    stu.moveToUpdateStudent=function(stuObj)
    {
        console.log('in moveToUpdateStudent function for Obj'+ stuObj);
        $state.go("updateStudent", {object:stuObj});

    };
}

})();
