(function () {
'use strict';

angular.module('trainingApp').controller('updatestudent.IndexController', Controller);

function Controller($window, $state, $scope, $filter, StudentService, CourseService, FlashService) {
	console.log('in Student update indexcontroller main function');

  
    var stu = this;
    stu.updateStudent = updateStudent;
    stu.findBatch = findBatch;
    stu.isCollapsed = true;
    stu.updInFirInsFee = updInFirInsFee;
    stu.updInSecInsFee = updInSecInsFee;
    stu.updInFirInsDate = updInFirInsDate;
    stu.updInSecInsDate = updInSecInsDate;
    console.log('in updatestudent controller ::'+$state.params.object._id);
  
    var firInsSubmDate = null;
    var secInsSubmDate = null;
    var firInsSubmFee = 0;
    var secInsSubmFee = 0;



    stu.updaObjId = $state.params.object._id;
    stu.inqid = $state.params.object.inquiryId;
    stu.candname = $state.params.object.name;
    stu.candmail = $state.params.object.email;
    stu.candnum = $state.params.object.phone;
    stu.loc = $state.params.object.loc;
    stu.course = $state.params.object.course;
    stu.fee= $state.params.object.fee;
    stu.feeSta= $state.params.object.feeSubmStatus;
    stu.actSubmFee= $state.params.object.actFeeAmt
    stu.feeSubmDate = $filter('date')(new Date($state.params.object.regDate), 'dd.MM.yyyy');
    stu.regisFee = $state.params.object.regFee;    
    console.log('$state.params.object.firInsDate '+$state.params.object.firInsDate);
    if ($state.params.object.firInsDate)
    {
      stu.firInsFee = $state.params.object.firInsFee;
      stu.firInsDate = $filter('date')(new Date($state.params.object.firInsDate), 'dd.MM.yyyy');
      firInsSubmDate = new Date($state.params.object.firInsDate);
      firInsSubmFee = stu.firInsFee;
    }
    else
    {
      stu.firInsFee = 0;
      stu.firInsDate = null; 
    }
    
    if($state.params.object.secInsDate)
    {
      stu.secInsFee = $state.params.object.secInsFee;
      stu.secInsDate = $filter('date')(new Date($state.params.object.secInsDate), 'dd.MM.yyyy');
      secInsSubmDate = new Date($state.params.object.secInsDate);
      secInsSubmFee = stu.secInsFee;
    }
    else
    {
      stu.secInsFee = 0;
      stu.secInsDate = null; 
    }

    stu.batStatus = $state.params.object.status;
    stu.batchId= $state.params.object.batchId;
    stu.traStatus = $state.params.object.traCompleted;
    stu.certIssu = $state.params.object.certifIssu;
    if($state.params.object.certifDate)
    {      
      stu.certifDate = $filter('date')(new Date($state.params.object.certifDate), 'dd.MM.yyyy');;
    }
    else
    {
      stu.certifDate = null;
    }
    stu.feedbaSubm = $state.params.object.feedbaSubm;
    stu.remarks = $state.params.object.remarks
  
    //Temp Variables for finding any change in Fee Status and Batch Id
    stu.preFeeStatus = stu.feeSta;
    stu.preBatchStatus = stu.batStatus;
    stu.oldBatchId     = stu.batchId;
    
    console.log('Old Batch Details: '+stu.preBatchStatus + ' and ' +stu.oldBatchId);
    console.log('Regis Date '+stu.feeSubmDate+'Fir Installment Date '+stu.firInsDate+'Sec Installment Date '+stu.secInsDate);


    stu.batStatusOptions = [{
           name: 'Batch Not Assigned',
           value: 'Batch Not Assigned'
        }, {
           name: 'Batch Assigned',
           value: 'Batch Assigned'
        }];

        stu.feeStatusOptions = [{
           name: 'Not Submitted',
           value: 'Not Submitted'
        }, {
           name: 'Full Amount Submitted',
           value: 'Full Amount Submitted'
        },{
           name: 'Partial Amount Submitted',
           value: 'Partial Amount Submitted'
        }];

        stu.traStatusOptions = [{
           name: 'Not Completed',
           value: 'Not Completed'
        }, {
           name: 'Completed',
           value: 'Completed'
        }];

 
    initController();

    // Fetch data from server using RESTful API
    function initController() {
        //Get Course Fee breakup from Course Service
        CourseService.GetByName(stu.course)
          .then(function (response) {   

              console.log('Success in Course GetByName');
              if(response!=null)
              {
                console.log('RegFee %age '+ response.regfee);
                console.log('1st Ins Fee ' + response.firinsfee);
                console.log('2nd Ins Fee' + response.secinsfee);

                stu.couRegFee = Math.round((stu.fee * response.regfee /100));
                stu.couFInsFee = Math.round((stu.fee  * response.firinsfee /100));
                stu.couSInsFee = Math.round((stu.fee  * response.secinsfee /100));
              }
              else
              {
                FlashService.Error('Course "'+stu.course+'" not found in Course Database.');   
              }
              
          })
          .catch(function (error) {
             console.log('Error in Course GetByName');
             FlashService.Error(error);
          });

          /*If Batch were already assigned then fetch latest batch list*/
          if(stu.batStatus==stu.batStatusOptions[1].value)
          {
              findBatch();
          }

    }
   
 
        stu.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };

        stu.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        stu.format = stu.formats[2];
        stu.altInputFormats = ['M!/d!/yyyy'];

        stu.open2 = function() {
          stu.popup2.opened = true;
        };


        stu.popup2 = {
          opened: false
        }; 

        stu.open3 = function() {
          stu.popup3.opened = true;
        };


        stu.popup3 = {
          opened: false
        }; 

        stu.open4 = function() {
          stu.popup4.opened = true;
        };


        stu.popup4 = {
          opened: false
        }; 

    /*function findBatch()
    {
      if (stu.batStatus==stu.batStatusOptions[1].value)
      {
      console.log('in findBatch function for Loc and Course ::'+ stu.loc + stu.course);
      StudentService.FindBatch(stu.loc, stu.course)
        .then(function (response) {
            console.log('Success in FindBatch ' + response);
            stu.batchList=response; 
        })
        .catch(function (error) {
           console.log('Error in findBatch');
           //FlashService.Error(error);
        });
      }
      else
      {
          console.log('Batch Status is Not Assigned');
          stu.batchList = null;
          stu.batchId = null; 
      }
    }*/


     function findBatch()
        {

          if((stu.preBatchStatus==stu.batStatusOptions[0].value) &&
           (stu.batStatus==stu.batStatusOptions[1].value))
          {
          console.log('in findBatch function for Loc and Course (with status change)::'+ stu.loc + stu.course);
          StudentService.FindBatch(stu.loc, stu.course)
            .then(function (response) {
                console.log('Success in FindBatch ' + response);
                stu.batchList=response;
                
                if (response.length >0)
                {
                    //stu.isFindBatch = true;
                    $window.alert('Batch found. Select suitable Batch Id.');
                }   
                else
                {
                  $window.alert('No Batch found. Wait for next Batch schedule.');
                  stu.batStatus=stu.batStatusOptions[0].value;
                } 
            })
            .catch(function (error) {
               console.log('Error in findBatch');
               //FlashService.Error(error);
               $window.alert('Error in finding Batch.');
               stu.batStatus=stu.batStatusOptions[0].value;
            });
          }
          else if ((stu.preBatchStatus==stu.batStatusOptions[1].value) && 
                  (stu.batStatus==stu.batStatusOptions[1].value))
          {
          console.log('in findBatch function for Loc and Course (without status change)::'+ stu.loc + stu.course);
          StudentService.FindBatch(stu.loc, stu.course)
            .then(function (response) {
                console.log('Success in FindBatch ' + response);
                stu.batchList=response; 
            })
            .catch(function (error) {
               console.log('Error in findBatch');
               //FlashService.Error(error);
            });
          }
          else
          {
            console.log('Batch Status is Not Assigned');            
            stu.batchList = null;
            stu.batchId = null;
          }
        }

    function updInFirInsFee()
    {
      console.log('in updInFirInsFee function');
      if (stu.firInsFee !=0)
      {
        calSubmFee();
        firInsSubmFee = stu.firInsFee;
      }
      else 
      {
        stu.firInsDate = null;
        firInsSubmDate = null;
        firInsSubmFee = 0;
      }
      console.log('firInsDate ' +stu.firInsDate);
    }

    function updInSecInsFee()
    {
      console.log('in updInSecInsFee function');
      if (stu.secInsFee !=0)
      {
        calSubmFee();
        secInsSubmFee = stu.secInsFee;
      }
      else 
      {
        stu.secInsDate = null;
        secInsSubmDate = null;
        secInsSubmFee=0;
      }
      console.log('secInsDate ' +stu.secInsDate);
    }

    function updInFirInsDate()
    {
      console.log('in updInFirInsDate function');
      if (stu.firInsDate)
      {
        firInsSubmDate = new Date(stu.firInsDate);

      }
      else 
      {
        firInsSubmDate = null;
      }
      console.log('stu.firInsDate '+stu.firInsDate + 'firInsSubmDate '+ firInsSubmDate);
    }

    function updInSecInsDate()
    {
      console.log('in updInSecInsDate function');
      if (stu.secInsDate)
      {
        secInsSubmDate = new Date(stu.secInsDate);
      }
      else 
      {
        secInsSubmDate = null;
      }
      console.log('stu.firInsDate '+stu.secInsDate + 'firInsSubmDate '+ secInsSubmDate);
    }

    function calSubmFee()
    {
        stu.actSubmFee = stu.regisFee + stu.firInsFee + stu.secInsFee;
    }

    function checkFeePaid()
        {
            var ret = true;
            var sum = 0;
            console.log('in checkFeePaid function for '+ stu.feeSta + ', ' + stu.actSubmFee + ', ' + stu.fee);
             // If Full Amnt Paid
             if ((stu.feeSta==stu.feeStatusOptions[1].value) && (stu.actSubmFee < stu.fee))
             {
                 $window.alert('Submitted fee INR '+stu.actSubmFee+' is less than Suggested Fee INR '+stu.fee+'. Check Fee Submission Status Or Submit Full Suggested Fee.');
                 ret = false;
             }
             // If Partial Amnt Paid
             else if (stu.feeSta==stu.feeStatusOptions[2].value)
             {                
                /*if((firInsSubmDate !=null) && (stu.firInsFee == 0) )
                {
                  $window.alert('Entered First Installment Fee INR '+stu.firInsFee+' is not valid. Check Course Fee Breakup for First Installment Fee ' + stu.couFInsFee + ' otherwise enter 0.');
                  ret = false;    
                }*/
                
                if((stu.firInsFee != 0) && (stu.firInsFee != stu.couFInsFee))
                {
                  $window.alert('Entered First Installment Fee INR '+stu.firInsFee+' is not valid. Check Course Fee Breakup for First Installment Fee ' + stu.couFInsFee + ' otherwise enter 0.');
                  ret = false;    
                }
                else if((stu.secInsFee != 0) && (stu.secInsFee != stu.couSInsFee))
                {
                  $window.alert('Entered Second Installment Fee INR '+stu.secInsFee+' is not valid. Check Course Fee Breakup for Second Installment Fee ' + stu.couSInsFee + ' otherwise enter 0.');
                  ret = false;    
                }
              
                if((stu.couRegFee +stu.couFInsFee + stu.couSInsFee) == (stu.regisFee + stu.firInsFee + stu.secInsFee))
                {
                  $window.alert('Full Amount Submitted hence change Fee Submission Status to "Full Amount Submitted".');
                  ret = false;    
                }
             }

             console.log('return :'+ret);
             return ret;             
        }

        function checkBatchAss()
        {
            var ret =true;
            console.log('in checkBatchAss function for stu batch status '+stu.batStatus);
            if(stu.batStatus == stu.batStatusOptions[1].value)
            {
              var sum = (stu.couRegFee + stu.couFInsFee);
              if(stu.actSubmFee < sum)
              {
                ret = false;
                $window.alert('Batch cannot be assigned. Either submit First Installment Fee INR '+stu.couFInsFee+' Or change Status to Batch Not Assigned.')
              }
              
            }
            console.log('return :'+ret);
            return ret;
        }

        function checkFeeStatus()
        {
          var ret = true;
          console.log('In checkFeeStatus function');
          if(stu.feeSta == stu.feeStatusOptions[0].value)
          {
                ret = false;
                $window.alert('Fee Submission Status as '+stu.feeSta+' is incorrect.');            
          }
          return ret;
        }

    function updateStudent()
    {
        
        var newBatchId = null;
        console.log('Old Id '+stu.oldBatchId + 'New Id ' +stu.batchId);
        if (checkFeePaid() && checkBatchAss() && checkFeeStatus())
        {
            // Update First & Second Installment Date if Submitted.
           // updateFeeDate();
        
            console.log('in updateStudent function for Student : '+ stu.candname );
            console.log('feeSubmStatus '+stu.feeSta + 'actFeeAmt' +stu.actSubmFee);
            console.log('firInsSubmFee '+ firInsSubmFee + ' firInsSubmDate '+firInsSubmDate);
            console.log('secInsSubmFee '+ secInsSubmFee + ' secInsSubmDate '+secInsSubmDate);

            if((stu.batStatus == "Batch Assigned") && (stu.oldBatchId != stu.batchId))
            {
               newBatchId = stu.batchId;
            }

            console.log('preBatchStatus '+ stu.preBatchStatus+ ' oldBatchId ' + stu.oldBatchId+' batch status ' + stu.batStatus + ' and newBatchId ' +newBatchId);
              StudentService.Update(stu.updaObjId, {
                feeSubmStatus:stu.feeSta,
                firInsFee:firInsSubmFee,
                firInsDate: firInsSubmDate,
                secInsFee:secInsSubmFee,
                secInsDate: secInsSubmDate,
                actFeeAmt:stu.actSubmFee,
                preBatchStatus: stu.preBatchStatus,
                oldBatchId: stu.oldBatchId, 
                status:stu.batStatus, 
                newBatchId:newBatchId,
                traCompleted:stu.traStatus,
                certifIssu:stu.certIssu,
                certifDate:stu.certifDate,
                feedbaSubm:stu.feedbaSubm,            
                remarks:stu.remarks
              })
              .then(function () {
                    FlashService.Success('Student '+stu.candname+' updated successfully!!!');
                })
                .catch(function (error) {
                   FlashService.Error(error);
                });
                //set preBatchStatus and OldBatchId to new status and new batch id
                stu.preBatchStatus = stu.batStatus;
                stu.oldBatchId     = stu.batchId
        }
      }
}

})();