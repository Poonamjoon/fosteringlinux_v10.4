(function () {
'use strict';

angular.module('trainingApp')
       .controller('createstudent.IndexController', Controller);

       
function Controller($window, $anchorScroll, $state, $filter, StudentService, CourseService, FlashService,inquiryservice) {
        var stu = this;

        /// Copy data from Followup to Enroll Student
        stu.inqid = $state.params.object.inqid;
        stu.candname = $state.params.object.stuname;
        stu.candmail = $state.params.object.stumail;
        stu.candnum = $state.params.object.stunum;
        stu.loc = $state.params.object.trgloc;
        stu.course = $state.params.object.trgcou;
        stu.fee= Math.round($state.params.object.suggfee);
        
        console.log('In createstudent controller for Student Name ' + stu.candname);
        console.log('Mail: '+ stu.candmail + ',Phone: '+ stu.candnum +',Loc: '+stu.loc +',Course: ' +stu.course );
        console.log('Suggested Fee :'+stu.fee);

        stu.batStatusOptions = [{
           name: 'Batch Not Assigned',
           value: 'Batch Not Assigned'
        }, {
           name: 'Batch Assigned',
           value: 'Batch Assigned'
        }];

        stu.feeStatusOptions = [{
           name: 'Not Submitted',
           value: 'Not Submitted'
        }, {
           name: 'Full Amount Submitted',
           value: 'Full Amount Submitted'
        },{
           name: 'Partial Amount Submitted',
           value: 'Partial Amount Submitted'
        }];

        stu.createStudent = createStudent;
        stu.findBatch = findBatch;
        stu.resetFeeFields = resetFeeFields;
        stu.calSubmFee = calSubmFee;
        stu.isEdit = false;
        stu.batchId = null;
        stu.batchList = null;
        stu.isCollapsed = true;
        var regisDate = null;        
        var firInsSubmDate = null;
        var secInsSubmDate = null;
        var regisFee = 0;
        var firInsSubmFee = 0;
        var secInsSubmFee = 0;
        
        stu.regisFee = 0;
        stu.firInsFee = 0;
        stu.actSubmFee = 0;

        stu.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };

        stu.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        stu.format = stu.formats[2];
        stu.altInputFormats = ['M!/d!/yyyy'];


        stu.open1 = function() {
          stu.popup1.opened = true;
        };

        stu.popup1 = {
          opened: false
        };  

        stu.open2 = function() {
          stu.popup2.opened = true;
        };


        stu.popup2 = {
          opened: false
        };  
 

        initController();

        // Fetch data from server using RESTful API
        function initController() {
            //Get Course Fee breakup from Course Service
            CourseService.GetByName(stu.course)
              .then(function (response) {   

                  console.log('Success in Course GetByName');
                  if(response!=null)
                  {
                    console.log('RegFee %age '+ response.regfee);
                    console.log('1st Ins Fee ' + response.firinsfee);
                    console.log('2nd Ins Fee' + response.secinsfee);

                    stu.couRegFee = Math.round((stu.fee * response.regfee /100));
                    stu.couFInsFee = Math.round((stu.fee  * response.firinsfee /100));
                    stu.couSInsFee = Math.round((stu.fee  * response.secinsfee /100));
                    console.log(stu.couRegFee);

                  }
                  else
                  {
                    FlashService.Error('Course "'+stu.course+'" not found in Course Database.');   
                  }
                  
              })
              .catch(function (error) {
                 console.log('Error in Course GetByName');
                 FlashService.Error(error);
              });
        }

        function findBatch()
        {
          if (stu.batStatus==stu.batStatusOptions[1].value)
          {
          console.log('in findBatch function for Loc and Course ::'+ stu.loc + stu.course);
          StudentService.FindBatch(stu.loc, stu.course)
            .then(function (response) {
                console.log('Success in FindBatch ' + response);
                stu.batchList=response;
                
                if (response.length >0)
                {
                    //stu.isFindBatch = true;
                    $window.alert('Batch found. Select suitable Batch Id.');
                }   
                else
                {
                  $window.alert('No Batch found. Wait for next Batch schedule.');
                  stu.batStatus=stu.batStatusOptions[0].value;
                } 
            })
            .catch(function (error) {
               console.log('Error in findBatch');
               //FlashService.Error(error);
               $window.alert('Error in finding Batch.');
               stu.batStatus=stu.batStatusOptions[0].value;
            });
          }
          else
          {
            console.log('Batch Status is Not Assigned');            
            stu.batchList = null;
            stu.batchId = null;
          }
        }

        function resetFeeFields()
        {
          stu.actSubmFee = 0;
          stu.regisFee = 0;
          stu.firInsFee = 0;
          stu.feeSubmDate= null;
          regisFee = 0;
          regisDate = null;
          firInsSubmFee = 0;
          firInsSubmDate = null;
          secInsSubmFee = 0;
          secInsSubmDate = null;
        }

        function calSubmFee()
        {
          stu.actSubmFee = stu.regisFee + stu.firInsFee;

          if(stu.firInsFee==0)
          {
             stu.firInsDate = null;
          }

          if(stu.regisFee==0)
          {
            stu.feeSubmDate = null;
          }

        }

        function updateFeeDate()
        {
            
            //In case of Full Fee Submitted,  Date will be calculated from Registration Date.
            //stu.regisDate = new Date(stu.feeSubmDate);   
            //regisDate = $filter('date')(new Date(stu.feeSubmDate), 'dd.MM.yyyy');                        
            regisDate = new Date(stu.feeSubmDate);

            if(stu.feeSta==stu.feeStatusOptions[2].value) 
            {
              regisFee = stu.regisFee;
              if( stu.firInsFee != 0)            
              {               
                //firInsSubmDate = new Date(stu.feeSubmDate);
                //firInsSubmDate = $filter('date')(new Date(stu.firInsDate), 'dd.MM.yyyy');
                firInsSubmDate = new Date(stu.firInsDate);
                firInsSubmFee = stu.firInsFee;
                
              }
            }
            else if(stu.feeSta==stu.feeStatusOptions[1].value)
            {
              regisFee = stu.couRegFee;
              firInsSubmFee = stu.couFInsFee;
              firInsSubmDate = regisDate;
              secInsSubmFee = stu.couSInsFee;
              secInsSubmDate =  regisDate; 
            }           
        }

        function checkFeePaid()
        {
            var ret = true;
            var sum = 0;
            console.log('in checkFeePaid function for '+ stu.feeSta + ', ' + stu.actSubmFee + ', ' + stu.fee);
             // If Full Amnt Paid
             if ((stu.feeSta==stu.feeStatusOptions[1].value) && (stu.actSubmFee < stu.fee))
             {
                 $window.alert('Submitted fee INR '+stu.actSubmFee+' is less than Suggested Fee INR '+stu.fee+'. Check Fee Submission Status Or Submit Full Suggested Fee.');
                 ret = false;
             }
             else if((stu.feeSta==stu.feeStatusOptions[1].value) && (stu.actSubmFee > stu.fee))
             {
                 $window.alert('Cannot submit Fee more than Suggested Fee INR '+stu.fee+'. Check Submitted fee INR '+stu.actSubmFee+'.');
                 ret = false;
             }
             // If Partial Amnt Paid
             else if (stu.feeSta==stu.feeStatusOptions[2].value)
             {                
                if (stu.regisFee != stu.couRegFee)
                {
                  $window.alert('Student cannot be enrolled. Please submit Registration Fee INR ' + stu.couRegFee +'.');
                  ret = false;    
                }
                else if((stu.firInsFee != 0) && (stu.firInsFee != stu.couFInsFee))
                {
                  $window.alert('Entered First Installment Fee INR '+stu.firInsFee+' is not valid. Submit Full First Installment Fee ' + stu.couFInsFee + ' otherwise enter 0.');
                  ret = false;    
                }

                if((stu.couRegFee +stu.couFInsFee + stu.couSInsFee) == (stu.regisFee + stu.firInsFee + stu.secInsFee))
                {
                  $window.alert('Full Amount Submitted hence change Fee Submission Status to "Full Amount Submitted".');
                  ret = false;    
                }

                /*else if ((stu.regisFee >= stu.couRegFee) && (stu.actSubmFee < stu.couFInsFee))
                {
                  sum = stu.couRegFee + stu.couFInsFee;
                  $window.alert('Entered Submitted Fee INR '+stu.actSubmFee+' is not valid. Submit either Registration Fee only or Full First Installment Fee (including Registration) INR ' + sum + '.');
                  ret = false;
                }
                else if ((stu.actSubmFee > stu.couFInsFee) && (stu.actSubmFee < stu.couSInsFee))
                {
                  sum = stu.couRegFee + stu.couFInsFee + stu.couSInsFee;
                  $window.alert('Entered Submitted Fee INR '+stu.actSubmFee+' is not valid. Submit full fee (including Registration Fee, First Installment Fee and Second Installment Fee) INR ' + sum + '.');

                  ret = false; 
                }
                else if(stu.actSubmFee == (stu.couRegFee + stu.couFInsFee + stu.couSInsFee))
                {
                  $window.alert('Change Fee Submission Status to "Full Amount Submitted" .');
                  ret = false;    

                }*/
             }

             console.log('return :'+ret);
             return ret;             
        }

        function checkBatchAss()
        {
            var ret =true;
            console.log('in checkBatchAss function for stu batch status '+stu.batStatus);
            if(stu.batStatus == stu.batStatusOptions[1].value)
            {
              var sum = (stu.couRegFee + stu.couFInsFee);
              if(stu.actSubmFee < sum)
              {
                ret = false;
                $window.alert('Batch cannot be assigned. Either submit First Installment Fee INR '+stu.couFInsFee+' Or change Status to Batch Not Assigned.')
              }
              
            }
            console.log('return :'+ret);
            return ret;
        }

        function createStudent() {
            console.log('in createStudent function for Inq Id : '+ stu.inqid + ' batch id ' + stu.batchId);

          if (checkFeePaid() && checkBatchAss())
          {
              // Update First Installment Date if Submitted.
              updateFeeDate();

            	StudentService.Add({
            		inquiryId:stu.inqid,
            		name:stu.candname,
                email:stu.candmail,
                phone:stu.candnum,
            		loc:stu.loc,
                course:stu.course,
                fee:stu.fee,
                feeSubmStatus:stu.feeSta,
                regFee:regisFee,
                regDate:regisDate,
                firInsFee:firInsSubmFee,
                firInsDate: firInsSubmDate,
                secInsFee:secInsSubmFee,
                secInsDate: secInsSubmDate,
                actFeeAmt:stu.actSubmFee,
                batchId:stu.batchId,
                status:stu.batStatus,
             		remarks:stu.remarks,
            	})
            	.then(function () {
                    FlashService.Success('Student '+stu.candname+' enrolled successfully!!!');
                    ////tejas
                    var inqstatus = "Enroll";


                   console.log(stu.inqid);
                    inquiryservice.StatusUpdate (stu.inqid,{
                        stat1:inqstatus              })
                })
                .catch(function (error) {

                   FlashService.Error(error);
                });
                
                stu.isEdit = true;
            }
            else{
              var inqstatus = "Open";
              console.log(stu.inqid);
              inquiryservice.StatusUpdate (stu.inqid,{
                  stat1:inqstatus              })
          }
        }
        $anchorScroll();
    }

})();
//ttp://www.infragistics.com/community/blogs/dhananjay_kumar/archive/2015/08/26/validating-user-input-on-a-form-in-angular-js.aspx