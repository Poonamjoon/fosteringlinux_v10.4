(function () {
'use strict';

angular.module('trainingApp')
       .controller('viewonestudent.IndexController', Controller);
       
function Controller($window, $state, StudentService, FlashService) {
        var stu = this;

        console.log('In viewonestudent controller ');

        stu.batStatusOptions = [{
           name: 'Batch Not Assigned',
           value: 'Batch Not Assigned'
        }, {
           name: 'Batch Assigned',
           value: 'Batch Assigned'
        }];

        stu.getStudent = getStudent;
        stu.cancel = cancel;
        stu.isEdit = false;
        stu.showDetails = false;

        function getStudent(mailid){
        console.log("in getStudent function for email id: "+mailid);
            StudentService.GetByEmail(mailid)
          .then(function (response) {
                if(response)
                {
                    $state.go("updateStudent", {object:response});
                }
                else
                {
                    $window.alert("No Student Found.");
                }
            })
            .catch(function (error) {
              console.log(error + 'in catch');
               FlashService.Error(error);
            });
        }

        function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
        }
    }

})();
//ttp://www.infragistics.com/community/blogs/dhananjay_kumar/archive/2015/08/26/validating-user-input-on-a-form-in-angular-js.aspx