



angular.module('trainingApp')
    .controller('studentreview.IndexController', Controller)

function Controller($state, $rootScope, inquiryservice, CourseService,$scope,StudentService,TrainerService,BatchService) {

    var strv = this;



   initController();
    function initController() {

        stuEmId = $rootScope.user.username;
        tk(stuEmId);
    }; 

    function tk(stu) {

        var date = new Date();
        strv.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);



        console.log("in getStudent function for email id: "+stu);
        StudentService.GetStuFeedBDet(stu)
            .then(function (response) {
                if(response)
                {
                 //   console.log(response);

                    strv.slis = [];
                    strv.slis = response;
                    strv.date = strv.FromDate;
                    strv.name = response.name;
                    strv.mail = response.email;
                    strv.id = response._id;
                    strv.center = response.loc;
                    strv.batchid = response.batchId;
//var tn = response.batchId;
                   
  //                  strv.Faculty = tn;


                  BatchService.GetByBatchId(strv.batchid)
                        .then(function (response) {
                            if (response)
                            {//console.log(response);

                                strv.Faculty = response.trainername;
                                strv.trainermail = response.trainer;

                       /*   TrainerService.getTrainerName(response.trainer)
                                    .then(function (response) {
                                        if (response)
                                        {
                                           // console.log(response);
                                            strv.Faculty = response.trainername;
                                             strv.trainerid = response._id;
                                            strv.trainermail = response.email;
                                        }

                                    });*/


                            }

                        });





                    strv.Course = response.course;
                    strv.ffm = response.remarks;



                    



                }
                else
                {alert("No Student Found.");
                }
            })
            .catch(function (error) {
                console.log(error + 'in catch');

            });

    };


/* this submit function is called/invoked when user clicks on submit button on student feedback page   */
    strv.submit = function () {

        if(strv.firstRate==undefined || strv.firstRate==0 || strv.secondRate==undefined || strv.secondRate==0 || strv.thirdRate==undefined || strv.thirdRate==0 || strv.fourthRate==undefined || strv.fourthRate==0 || strv.fifthRate==undefined || strv.fifthRate==0 || strv.sixthRate==undefined || strv.sixthRate==0 || strv.seventhRate==undefined || strv.seventhRate==0 || strv.eightRate==undefined || strv.eightRate==0 || strv.ninthRate==undefined || strv.ninthRate==0 || strv.tenthRate==undefined || strv.tenthRate==0 || strv.elevenRate==undefined || strv.elevenRate==0 || strv.twelveRate==undefined || strv.twelveRate==0 || strv.thirteenRate==undefined || strv.thirteenRate==0 || strv.fourteenRate==undefined || strv.fourteenRate==0 || strv.fifteenRate==null || strv.sixteenRate==undefined || strv.sixteenRate==0 || strv.seventeenRate==undefined || strv.seventeenRate==0 || strv.eighteenRate==undefined || strv.eighteenRate==0 || strv.nineteenRate==null){

            alert("!!! Please Review All The Questions !!!");

            return;
        }

     
        /* declaring the arrays which stores data from student feedback screen like submit data , rating by the
         users , faculty group rating , infra group rating , support group rating , total rating */
        var submitdata = []; var rating =[];var faculty=0;var infra=0;var support =0;var tot = 0;

        submitdata.push(strv.firstRate,strv.secondRate,strv.thirdRate,strv.fourthRate,strv.fifthRate,strv.sixthRate,strv.seventhRate,strv.eightRate,strv.ninthRate,strv.tenthRate,strv.elevenRate,strv.twelveRate,strv.thirteenRate,strv.fourteenRate,strv.fifteenRate*1,strv.sixteenRate,strv.seventeenRate,strv.eighteenRate,strv.nineteenRate*1);


        for(i=0;i<19;i++){
            if(submitdata[i]==0){rating.push(0);}
            if(submitdata[i]==1){rating.push(2);}
            if(submitdata[i]==2){rating.push(5);}
            if(submitdata[i]==3){rating.push(8);}
            if(submitdata[i]==4){rating.push(10);}
        }



        console.log(submitdata);




        for(i=0;i<8;i++){faculty += rating[i];}rating.push(faculty);
        for(i=8;i<16;i++){infra += rating[i];}rating.push(infra);
        for(i=16;i<19;i++){support += rating[i];}rating.push(support);
        for(i=0;i<19;i++){ tot += rating[i];}  rating.push(tot);


    



        console.log(strv.firstRate);

      /* This service inserts the student feedback data into the studentfeedback table/collections in db */

       StudentService.CreateFeedbackData({


           studentid:strv.id,
            batchid:strv.batchid,
            trainername:strv.Faculty,
            trainermail:strv.trainermail,
            studname:strv.name,
            studmail:  strv.mail,
            studcenter:strv.center,
            studcourse:strv.Course,
            revdate:strv.FromDate,
            ratfaculty:faculty,
            ratinfra:infra,
            ratsupport:support,
            totrating:tot

            
            
            
            
        });


        alert("!!! Thanks For Reviewing !!!");

    }



}