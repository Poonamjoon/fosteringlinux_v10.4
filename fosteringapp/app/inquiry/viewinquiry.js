angular.module('trainingApp').controller('inqviewall.IndexController', Controller);

function Controller($window, inquiryservice, FlashService, $state, $filter, ngTableParams) {
	console.log('in inqviewallcontroller main function');

	var inq = this;
	
  var response=[];
  var tableData = [] ;
  var orderedData=[];
  console.log('in inqviewallcontroller function');

  inq.isVisible = false;
  inq.ShowHide = function () { 
              //If DIV is visible it will be hidden and vice versa.
              inq.isVisible = inq.showCol;
              console.log('in ShowHide function: showCol :' + inq.showCol + 'and isVisible :' +inq.isVisible);
          }

    initController();

   // Fetch data from server using RESTful API
  function initController() {
  inquiryservice.getData().then(function(response) {
        console.log ('Success in getData');
        tableData = response;
        console.log (tableData);

        //Table configuration
        inq.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { candname: "asc"},
        filter: {candname: ''}    // initial filter
        },{
        total:tableData.length,

        //Returns the data for rendering
        getData : function($defer,params){
        var filteredData = $filter('filter')(tableData, params.filter());
        console.log(filteredData);

        orderedData = $filter('orderBy')(filteredData, params.orderBy());
        console.log(orderedData);

        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        params.total(orderedData.length);
        }
        });
    })
    .catch(function (error) {
        console.log('Error in getData');
        FlashService.Error(error);
    });
};

/*inq.toggleMore = function(){
      for(var i=0;i<2;i+=1){
        var col = inq.viewCols[i];
        col.show(!col.show());
      }
    };
*/
inq.createStudent =function(id, name, mail, phone, loc, course, fee,status) {
    console.log('inqviewallcontroller:in createStudent function '+ id + ' ,' + name + ', '+course);
    
    if(course=="Others")
    {
        $window.alert("Student cannot be enrolled for Others Course.");
    }

      var obj = {inqid: id, stuname: name, stumail: mail, stunum:phone, trgloc: loc, trgcou: course, suggfee: fee,status:status};

        if(obj.status=="Enroll")
        {
            var msg = "!!! Student Is Already Enrolled !!!";
            alert(msg);
            return;
        }
        if(obj.status=="Reject")
        {
            var msg = "\t\tStudent's Inquiry Is Rejected , Can't Be Enrolled.";
            alert(msg);
            return;
        }
        else{
      $state.go("EnrollStudent", {object:obj});
        }

  }

  inq.getInquiry=function()
    {
        console.log('in getInquiry func')
        inquiryservice.GetById[$stateParams.id];
    };
    
};

//http://stackoverflow.com/questions/21601154/angularjs-dynamic-rows-and-columns-with-a-two-way-bound-custom-control
