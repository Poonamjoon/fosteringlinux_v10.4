/**
 * Created by tejas on 10-06-2016.
 */
angular.module('trainingApp')





	//fostering app inquery submitter
	.controller('inqsubmit.IndexController', Controller)

	.directive('numbersOnly', function () {
		return {
			require: 'ngModel',
			link: function (scope, element, attr, ngModelCtrl) {
				function fromUser(text) {
					if (text) {
						var transformedInput = text.replace(/[^0-9]/g, '');

						if (transformedInput !== text) {
							ngModelCtrl.$setViewValue(transformedInput);
							ngModelCtrl.$render();
						}
						return transformedInput;
					}
					return undefined;
				}
				ngModelCtrl.$parsers.push(fromUser);
			}
		};
	});

function Controller($window, $state, $http, inquiryservice, FlashService, CourseService, $scope, clientmail) {
	console.log('in inquiry submitindexcontroller main function');
	var inq = this;
	inq.courseList=[];


	inq.displayTime = new Date();
	inq.displayTime2 = new Date();

	inq.ismeridian = false;
	inq.meridian = ['AM', 'PM'];

	inq.startTime = 'n/a';
	inq.endTime = 'n/a';

	$scope.$watch('inq.displayTime', function(newValue, oldValue) {
		var hour = inq.displayTime.getHours()-(inq.displayTime.getHours() >= 12 ? 12 : 0),
			hour = hour<10 ? '0'+hour : hour,
			minutes = (inq.displayTime.getMinutes()<10 ? '0' :'') + inq.displayTime.getMinutes(),
			period = inq.	displayTime.getHours() >= 12 ? 'PM' : 'AM';
		inq.startTime = hour+':'+minutes+' '+period
	});

	$scope.$watch('inq.displayTime2', function(newValue, oldValue) {
		var hour = inq.displayTime2.getHours()-(inq.displayTime2.getHours() >= 12 ? 12 : 0),
			hour = hour<10 ? '0'+hour : hour,
			minutes = (inq.displayTime2.getMinutes()<10 ? '0' :'') + inq.displayTime2.getMinutes(),
			period = inq.displayTime2.getHours() >= 12 ? 'PM' : 'AM';
		inq.endTime = hour+':'+minutes+' '+period
	});

//Get Course List from Course Service

	CourseService.GetAll()
		.then(function (response) {
			console.log('Success in Course GetAll');
			inq.courseList=response;


		})
		.catch(function (error) {
			FlashService.Error(error);
		});






	/*$scope.getfee = function (item) {
	 if(item=="Others"){
	 inq.fosc=0;
	 inq.dosc=0;
	 inq.destrg="Others";}
	 //console.log(item);
	 else{
	 var x = item;
	 //console.log(x);
	 var s = x.toString();
	 //console.log(s);
	 var values = s.split('_');


	 var totfee = values[0];
	 var durations = values[1];
	 var name = values[2];
	 //console.log(totfee);
	 //console.log(durations);
	 //console.log(item);
	 inq.fosc=totfee;
	 inq.dosc=durations;
	 inq.destrg = name;




	 }
	 }*/

	$scope.getfee = function (item) {
		if (item=="Others") {
			inq.fosc = 0;
			inq.dosc = 0;
            inq.netfee = null; 
            inq.dio = null; 
            inq.difee = null; 

        } else {

			var result = [];
			var searchfield = "name";
			var searchval = item;
			for (i = 0; i < inq.courseList.length; i++) {

				if (inq.courseList[i][searchfield] == searchval) {
					inq.fosc = inq.courseList[i].fee;
					console.log(inq.courseList[i].fee);
					inq.dosc = inq.courseList[i].duration;
					console.log(inq.courseList[i].duration);
					inq.stax = inq.courseList[i].srvctax;
					console.log(inq.stax = inq.courseList[i].srvctax);

					inq.netfee =  inq.fosc + ((inq.fosc)*inq.stax/100);
				}
			}

		}
	}
	$scope.nefee = function () {
		console.log("dis"+inq.dio);
		var afee ="";
		var pfee = inq.fosc *1 ;
		var d = inq.dio *1;
		var st = inq.stax *1;



		if(inq.dio==0){

		inq.difee = (pfee * (1-(d/100)) );


			inq.netfee =   Math.round((((inq.difee )*(1+(st/100)))) * 100) / 100;





		}

		else{


			inq.difee = (pfee * (1-(d/100)) );




			inq.netfee =   Math.round((((inq.difee )*(1+(st/100)))) * 100) / 100;




		}

	}













	$scope.myVal1 = true;


	inq.reviewInquiry = function () {





		var isrc = document.getElementById("inqsrc").value; if (isrc=="") {
			alert("!!!! Inquiry source field in Inquiry Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return; }

		var inqfor = document.getElementById("inqfor").value;
		var inqfor1 = document.getElementById("inqffor1").value;
		var inqfor2 = document.getElementById("inqffor2").value;
		var inqfor3 = document.getElementById("inqffor3").value;

		if (inqfor=="") {
			alert("!!!! Inquiry for field in Inquiry Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return; }

		if(inqfor=="Certification" && inqfor1 == "!!!!training done in Institute Details cannot be blank !!!!" )
		{
			alert("!!!! Institute Details in Inquiry Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;


		}
		if (inqfor=="Certification" && inqfor1 == "Other" && inqfor2=="") {

			alert("!!!! Institute Details in Inquiry Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;}



		var ihandler = document.getElementById("inqhandler").value;
		if (ihandler==""){
			alert("!!!! Person handling inquiry in Inquiry Details Cannot Be Blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;
		}

		var cdnm = document.getElementById("candname1").value;
		if (cdnm==""){
			alert("!!!! Candidate Name in Candidate Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;
		}
		var cdcn1 = document.getElementById("candnum1").value;
		if (cdcn1==""){
			alert("!!!! Contact Number 1 in Candidate Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;
		}
		if(cdcn1.length !=10)
		{
			alert("!!!! Contact Number 1 in Candidate Details is invalid !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;

		}

		var cdeml = document.getElementById("candmail").value;
		if (cdeml=="") {
			alert("!!!! Candidate Email in Candidate Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}

		var atpos = cdeml.indexOf("@");
		var dotpos = cdeml.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=cdeml.length) {
			alert("!!!! Candidate Email Is Not a valid e-mail address !!!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;
		}



		var inqeduclass11 = document.getElementById("inqeduclass1").value;

		if (inqeduclass11=="") {
			alert("!!!! Class in Educational Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}

		var eduname11 = document.getElementById("eduname1").value;
		if (eduname11=="") {
			alert("!!!! Name of Institute in Educational Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}
		var eduloc11 = document.getElementById("eduloc1").value;
		if (eduloc11=="") {
			alert("!!!! Location of Institute in Educational Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;

		}

		var eduyear11 = document.getElementById("eduyear1").value;
		if (eduyear11=="") {
			alert("!!!! Year of Passing in Educational Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}

        
        else if (eduyear11 < 1970 || eduyear11 > 9999) {
            alert("!!!!  Year of Passing in Educational Details is wrong !!!");
            $scope.myVal = false;
            $scope.myVal1 = true;
            return;
        } else {}

		var ditc = document.getElementById("inqdestrg").value;

		if (ditc=="? undefined:undefined ?") {
			alert("!!!! Desired Training Course In Training Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}

		var othecrse = document.getElementById("othcrse").value;
		if (ditc=="Others" && othecrse==""   ) {
			alert("!!!! New Training Course In Training Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}



		var trm = document.getElementById("inqmonth").value;

		if (trm=="") {
			alert("!!!! Training Month In Training Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;

		}



		var tday = document.getElementById("inqdesday").value;
		if (tday==""){
			alert("!!!! Desired Training Day In Training Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;
		}

		var tloc = document.getElementById("inqdesloc").value;
		if (tloc=="") {
			alert("!!!! Desired Training Location In Training Details cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;
			return;

		}

		var tdio = document.getElementById("dio").value;
		var tda = document.getElementById("da").value;
		if (tdio>0 && tda=="" && ditc!="Others" ) {

			alert("!!!! Person Name For Discount Approved In Training Details Cannot be blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}
		if (tdio=="" && ditc!="Others") {

			alert("!!!! Discount Offered In Training Details Cannot Be Blank !!!");
			$scope.myVal = false;
			$scope.myVal1 = true;

			return;
		}




		$scope.myVal = true;
		window.scrollTo(0, 0);

		$scope.myVal1 = false;


	}

//function to redirect back to inquiry page from viewpage
	inq.rdirect = function () {
		$scope.myVal = false;
		$scope.myVal1 = true;
		alert("You are being redirected to the inquiry details page");

	}

	//function to reset input fields values
	inq.rese = function () {


		 alert("Your input values are getting reset");
		 inq.src=null;
		 inq.srcref=null;
		 inq.for=null;
		 inq.loc=null;
		 inq.nature=null;
		 inq.corpname=null;
		 inq.date=null;
		 inq.handler=null;
		 inq.candname=null;
		 inq.candnum1=null;
		 inq.candnum2=null;
		 inq.candmail=null;
		 inq.candlinkin=null;
		 inq.candfacebook=null;
		 inq.candtwitter=null;
		 inq.educlass1=null;
		 inq.eduname1=null;
		 inq.eduloc1=null;
		 inq.eduyear1=null;
		 inq.educlass2=null;
		 inq.eduname2=null;
		 inq.eduloc2=null;
		 inq.eduyear2=null;
		 inq.educlass3=null;
		 inq.eduname3=null;
		 inq.eduloc3=null;
		 inq.eduyear3=null;
		 inq.exp=null;
		 inq.genexp=null;
		 inq.jobexp1=null;
		 inq.jobdet1=null;
		 inq.jobloc1=null;
		 inq.joblinux1=null;
		 inq.jobexp2=null;
		 inq.jobdet2=null;
		 inq.jobloc2=null;
		 inq.joblinux2=null;
		 inq.destrg=null;
		 inq.startTime =null;
		 inq.endTime = null;
		 inq.desday=null;
		 inq.desmon=null;
		 inq.desloc=null;
		 inq.othCou=null;
		 inq.othfee=null;
		 inq.othdur=null;
		 inq.fosc=null;
		 inq.dosc=null;
		 inq.stax=null;
		 inq.dio = null;
		inq.difee=null;
		 inq.da=null;
		 inq.netfee = null;
		 inq.followupDate1=null;
		 inq.statusofInquiry=null;
		 inq.convinquiry = null;
		 inq.counsellorRemarks=null;
		 inq.sumofdis = null;

    };

	inq.submitInquiry =function() {



		var date = new Date();

		$scope.FromDate = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();

		inq.followdate1 = "";
		inq.rem1 = "";
		inq.stat1 = "Open";
		inq.followdate2 = "";

		inq.rem2 = "";
		inq.followdate3="";
		inq.rem3="";



		console.log('submitindexcontroller:in submitInquiry function');
		inquiryservice.createData ({
			src:inq.src,
			srcref:inq.srcref,
			for:inq.for,
			for1:inq.for1,
			for2:inq.for2,
			for3:inq.for3,
			loc:inq.loc,
			nature:inq.nature="Retail",
			corpname:inq.corpname,
			date:inq.date=$scope.FromDate,
			handler:inq.handler,
			candname:inq.candname,
			candnum1:inq.candnum1,
			candnum2:inq.candnum2,
			candmail:inq.candmail,
			candlinkin:inq.candlinkin,
			candfacebook:inq.candfacebook,
			candtwitter:inq.candtwitter,
			educlass1:inq.educlass1,
			eduname1:inq.eduname1,
			eduloc1:inq.eduloc1,
			eduyear1:inq.eduyear1,
			educlass2:inq.educlass2,
			eduname2:inq.eduname2,
			eduloc2:inq.eduloc2,
			eduyear2:inq.eduyear2,
			educlass3:inq.educlass3,
			eduname3:inq.eduname3,
			eduloc3:inq.eduloc3,
			eduyear3:inq.eduyear3,
			exp:inq.exp,
			genexp:inq.genexp,
			jobexp1:inq.jobexp1,
			jobdet1:inq.jobdet1,
			jobloc1:inq.jobloc1,
			joblinux1:inq.joblinux1,
			jobexp2:inq.jobexp2,
			jobdet2:inq.jobdet2,
			jobloc2:inq.jobloc2,
			joblinux2:inq.joblinux2,
			destrg:inq.destrg,
			othCou:inq.othCou,
			othfee:inq.othfee,
			othdur:inq.othdur,
			desmon:inq.desmon,
			startTime:inq.startTime ,
			endTime:inq.endTime ,
			desday:inq.desday,
			desloc:inq.desloc,

			fosc:inq.fosc,
			dosc:inq.dosc,
			stax:inq.stax,
			dio:inq.dio,
			difee:inq.difee,
			da:inq.da,
			netfee:inq.netfee,

			followdate1:inq.followdate1,
			rem1:inq.rem1,
			stat1:inq.stat1,

			followdate2:inq.followdate2,
			rem2:inq.rem2,

			followdate3:inq.followdate3,
			rem3:inq.rem3,


			sumofdis:inq.sumofdis

			/* todo: define model and work out on some array etc */

            })
            .then(function() {
                FlashService.Success('Inquiry submitted successfully !!!');
            })
            .catch(function(error) {
                console.log('Error from db ' + error);
                FlashService.Error("Your Email id is already registered");
		});

        /*****************************************************************/

        var mcourse = "";
        var mfee = "";
        var mdur = "";
        var stx = "";
        var nf = "";
        if (inq.destrg == "Others") {
            mcourse = inq.othCou;
            mfee = inq.othfee;
            mdur = inq.othdur;
            stx = "NA";
            nf = inq.fosc;

        } else {
            mcourse = inq.destrg;
            mfee = inq.fosc;
            mdur = inq.dosc;
            stx = inq.stax;
            nf = inq.netfee;

		}
		var mda = "";
        if (inq.dio == 0) {
            mda = "NA";
        } else {
			mda=inq.da;
		}


		clientmail.getmailinfo().then(function(response) {
			console.log ('Success in getData');
			console.log(response);
			inq.usrls = [];
			usrls = response;
		$scope.frmmail = usrls[0].username;
			$scope.frmpass = usrls[0].password;
		})
		var	data= {
			frmmail:"admin_training@fosteringlinux.com",
			frmpass:"@DmnTrfl16",
			to: inq.candmail,
			name: inq.candname,
			inqfor: inq.for,
			by: inq.handler,
			course: mcourse,
			fee:mfee,
			dur:mdur,
			month:inq.desmon,
			st:inq.startTime,
			et:inq.endTime,
			day:inq.desday,
			loc:inq.desloc,
			se:stx,
			dio:inq.dio,
			da:mda,
			nfe:nf

		}
		console.log(data);

		clientmail.submitinquirym(data)
			.then(function () {
                console.log("last step of calling submitInquiry");
				alert("mail sent succesfully");


			})
	/*	$http.post('/submitinquirym',data).success(function (response) {

				alert("mail sent succesfully");

				// alert("Premium Rate: "+ val_rate + "\n" + "Premium Amount: INR "+premium_amt);


			}

		)*/


		$scope.myVal = false;
		window.scrollTo(0, 0);
		$scope.myVal1 = true;
		var moveToAddCou = false;
		if (inq.destrg == 'Others')
		{
            var r = confirm("You have selected course as Others. Do you like to add course :" + inq.othCou + "?");
			if (r == true) {
				console.log('pressed OK to add new course ' + inq.othCour);
				moveToAddCou = true;
			} else {
				console.log('pressed CANCEL to ignore Other course' + inq.othCour);
			}
		}











		 inq.src=null;
		 inq.srcref=null;
		 inq.for=null;
		 inq.loc=null;
		 inq.nature=null;
		 inq.corpname=null;
		 inq.date=null;
		 inq.handler=null;
		 inq.candname=null;
		 inq.candnum1=null;
		 inq.candnum2=null;
		 inq.candmail=null;
		 inq.candlinkin=null;
		 inq.candfacebook=null;
		 inq.candtwitter=null;
		 inq.educlass1=null;
		 inq.eduname1=null;
		 inq.eduloc1=null;
		 inq.eduyear1=null;
		 inq.educlass2=null;
		 inq.eduname2=null;
		 inq.eduloc2=null;
		 inq.eduyear2=null;
		 inq.educlass3=null;
		 inq.eduname3=null;
		 inq.eduloc3=null;
		 inq.eduyear3=null;
		 inq.exp=null;
		 inq.genexp=null;
		 inq.jobexp1=null;
		 inq.jobdet1=null;
		 inq.jobloc1=null;
		 inq.joblinux1=null;
		 inq.jobexp2=null;
		 inq.jobdet2=null;
		 inq.jobloc2=null;
		 inq.joblinux2=null;
		 inq.destrg=null;
		 inq.startTime = null;
		 inq.endTime = null;
		 inq.desday=null;
		 inq.desmon=null;
		 inq.desloc=null;
		 inq.othCou=null;
		 inq.othfee=null;
		 inq.othdur=null;
		 inq.fosc=null;
		 inq.dosc=null;
		 inq.stax=null;
		 inq.dio = null;
		inq.difee=null;
		 inq.da=null,
		 inq.netfee=null;
		 inq.followupDate1=null;
		 inq.statusofInquiry=null;
		 inq.convinquiry = null;
		 inq.counsellorRemarks=null;
		 inq.sumofdis=null;

		if (moveToAddCou)
		{
			$state.go("addNewCourse");
		}

	};


	inq.cancel = function () {

		alert("Redirecting Back to Dashboard");

    };


    inq.getData=function(email){
        console.log("in getInquiry function for email id: " + email);
        inquiryservice.getByEmail(email)
            .then(function(response) {
                if (response) {
                    console.log("Email id is found:" + response);
                      $scope.mailfound=response;
                } else {
                    $window.alert("No Inquiry Found.");
	}
            })
            .catch(function(error) {
                $window.alert(error);
            });
    }

};

