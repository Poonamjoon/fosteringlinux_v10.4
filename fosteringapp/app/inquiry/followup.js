/**
 * Created by tejas on 20-07-2016.
 */



angular.module('trainingApp')
    .controller('inquiryfollowup.IndexController', Controller)

function Controller($state,inquiryservice, CourseService,$scope,inquirydata) {
    
    var inqf = this;


    

    if(inquirydata.destrg=="Others"){



        var msg = "Follow Up For This Inquiry Cannot Be Done"+"\n\n"+"!!! You Are Being" +
            " Redirect" +
            " From" +
            " This" +
            " Page !!!";
        alert(msg);
        window.location = "/app/#/ViewAllInquiries";
    }
    else {inqf.Course = inquirydata.destrg;}
    

    inqf.objectid = inquirydata._id ;
  
    inqf.id = inquirydata.id;

    inqf.name = inquirydata.candname;
    inqf.Email = inquirydata.candmail;

    
    inqf.Location = inquirydata.desloc;




    inqf.followdate1 = inquirydata.followdate1;

    if(inquirydata.followdate1!=null){
        $scope.dtbox11 = "true";
        $scope.dtbox12 = "true";
        $scope.rmbox1 = "true";
        
    }



    inqf.rem1 = inquirydata.rem1;
    inqf.followdate2 = inquirydata.followdate2;


    if(inquirydata.followdate2!=null){
        $scope.dtbox21 = "true";  //hide
        $scope.dtbox22 = "true";
        $scope.rmbox2 = "true";

    }
    inqf.rem2 = inquirydata.rem2;
    inqf.followdate3 = inquirydata.followdate3;
    if(inquirydata.followdate3!=null){
        $scope.dtbox31 = "true";
        $scope.dtbox32 = "true";
        $scope.rmbox3 = "true";

    }

    inqf.rem3 = inquirydata.rem3;
    inqf.stat1 = inquirydata.stat1;
     



    inqf.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    inqf.open1 = function() {
        inqf.popup1.opened = true;
    };
    inqf.open2 = function() {
        inqf.popup2.opened = true;
    };
    inqf.open3 = function() {
        inqf.popup3.opened = true;
    };


    inqf.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    inqf.format = inqf.formats[2];
    inqf.altInputFormats = ['M!/d!/yyyy'];

    inqf.popup1 = {
        opened: false
    };
    inqf.popup2 = {
        opened: false
    };
    inqf.popup3 = {
        opened: false
    };

inqf.createStudent = function () {

    console.log('Netfee :: '+inquirydata.netfee);
    var obj = {inqid: inquirydata._id, stuname: inquirydata.candname, stumail: inquirydata.candmail, stunum:inquirydata.candnum1, trgloc: inquirydata.desloc, trgcou: inquirydata.destrg, suggfee: inquirydata.netfee,status:inquirydata.stat1};
    if(inquirydata.stat1=="Enroll"){alert("!!! Student Is Already Enrolled !!!");
        return;
    }
    $state.go("EnrollStudent", {object:obj});
}
    inqf.followinquiry = function () {




        var f1 = document.getElementById("followdate1").value;
        if (f1=="" && inqf.followdate1==null) {
            alert("!!!! Follow Up 1 Date cannot be blank !!!");

            return; }

        var f3 = document.getElementById("rem1").value;
        if (f3=="") {
            alert("!!!! Remarks 1 cannot be blank !!!");

            return; }

        var dbInqStaus =  null;
        var set = null;

        if(inqf.stat1=="Enroll")
        { 
            //fetch inquiry status from DB and check whether Student is enrolled or not.
            inquiryservice.GetInqStatus(inquirydata._id)
            .then(function (response) {
                console.log('Success in GetInqStatus');
                dbInqStaus=response;                
            })
            .catch(function (error) {
               console.log(error);
            });

            if (dbInqStaus!="Enroll")
            {
                alert("Before submitting Follow-up details, Please Enroll Student by clicking on Enroll button.");
                return;
            }
        }

        console.log('in followinquiry:followinquiry function');
   
        

        if (dbInqStaus=="Enroll")
        {
            //No need to update Status as it is already updated through Create Student
            set  = {
                id:inqf.id,
                followdate1: inqf.followdate1,
                rem1: inqf.rem1,             
                followdate2: inqf.followdate2,
                rem2: inqf.rem2,   
                followdate3: inqf.followdate3,
                rem3: inqf. rem3
                };
        }
        else
        {
            set  = {
                id:inqf.id,
                followdate1: inqf.followdate1,
                rem1: inqf.rem1,
                stat1: inqf.stat1,
                followdate2: inqf.followdate2,
                rem2: inqf.rem2,
               
                followdate3: inqf.followdate3,
                rem3: inqf. rem3
               
            };   
        }
                
            
        inquiryservice.FollowUpdate ( inqf.objectid,set)
            .then(function () {
                alert('Follow Up Inquiry updated successfully!!!');
               
                alert("!!!You are being redirected to View Inquiry Page!!!");
                window.location = "/app/#/ViewAllInquiries";


            })
            .catch(function (error) {
                console.log('Error from db ' + error);

                console.log(error.code);
                if (error.code == 11000)
                {
                    var field = error.message.split('.$')[1];
                    field = field.split(' dup key')[0];
                    field = field.substring(0, field.lastIndexOf('_'));

                    console.log('field : ' + field);
                    var msg;
                    if(field=="id")
                        msg="Failed to Update Follow Up Inquiry. Duplicate Inquiry ID found !";
                    else
                        msg="Failed to Update Follow Up. Duplicate Inquiry Name found !";

                    alert(msg);

                }
                else
                {
                    console.log(error);
                    alert(error);
                }

            });

    };
    inqf.getInquiry=function()
    {
        console.log('in getInquiry func')
        return inquiryservice.Get[$stateParams.id];
    };


}
