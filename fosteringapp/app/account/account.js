﻿(function () {
    'use strict';

    angular
        .module('trainingApp')
        .controller('account.IndexController', Controller);


    function Controller($window, $rootScope, UserService, FlashService) {
    console.log('in account index controller controller function');

        var acc = this;

        console.log($rootScope.user.username);
        acc.user = $rootScope.user;
        acc.email = acc.user.username;
        acc.update = update;
        acc.cancel = cancel;

        function update() {
console.log('in account index controller update function');


            acc.user.oldPwd = acc.oldPasswd;
            acc.user.newPwd = acc.newPasswd;
            acc.user.cnfNewPwd = acc.cnfNewPasswd;

            console.log(acc.user);
            
            UserService.Update(acc.user)
                .then(function () {
                    acc.oldPasswd = null;
                    acc.newPasswd = null;
                    acc.cnfNewPasswd = null;
                                        
                    $window.alert('Passowrd updated successfully. Please login again with new Passowrd!!!');
                    $window.location.href = '/login';
                })
                .catch(function (error) {
                    FlashService.Error(error);
                });
        }

        function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
        }

    }

})();
