(function () {
'use strict';

angular.module('trainingApp')
       .controller('register.IndexController', Controller);
       
function Controller($window, UserService, FlashService) {


        var admin = this;

        console.log('in Register controller');
        admin.create = create;
        admin.cancel = cancel;

        function create() {
            console.log('in create function');
            console.log(admin.firNam + admin.lasNam + admin.email + admin.passwd + admin.role);
            UserService.Register({
                    firstName:admin.firNam,
                    lastName:admin.lasNam,
                    username:admin.email,
                    password:admin.passwd,
                    role:admin.role
                })
                .then(function () {
                    FlashService.Success('User added successfully!!!');
                    admin.firNam = null;
                    admin.lasNam = null;
                    admin.email = null;
                    admin.passwd = null;
                    admin.role = null;
                })
                .catch(function (error) {
                   FlashService.Error(error);
                });

        }

    
        function cancel() {
            console.log('in cancel function');
            $window.location.href = "/app";
        }
    }
})();
