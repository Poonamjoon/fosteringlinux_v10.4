(function () {
'use strict';

angular.module('trainingApp')
       .controller('viewuser.IndexController', ViewController)

function ViewController($window, $rootScope, UserService, FlashService, $filter, ngTableParams) {
    var admin = this;
    var response=[];
    var tableData = [] ;
    admin.movetoUpdate = movetoUpdate;
    admin.cancel = cancel;
    admin.deleteuser = deleteuser;
    
    console.log('in ViewController function');

    admin.isVisible = false;
    admin.ShowHide = function () { 
      //If DIV is visible it will be hidden and vice versa.
      admin.isVisible = admin.showCol;
      console.log('in ShowHide function: showCol :' + admin.showCol + ' and isVisible :' +admin.isVisible);
    }

    initController();

    // Fetch data from server using RESTful API
    function initController() {
    UserService.GetAll().then(function(response) {
        tableData = response;
        console.log (tableData);

        //Table configuration
        admin.tableParams = new ngTableParams({
        page: 1,
        count: 10,
        sorting: { firstName: "asc" },
        //filter: {firstName: '' }       // initial filter
        },{
            total:tableData.length,
            //Returns the data for rendering

            getData : function($defer,params){
            var filteredData = $filter('filter')(tableData, params.filter());
            console.log(filteredData);

            var orderedData = $filter('orderBy')(filteredData, params.orderBy());
            console.log(orderedData);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            params.total(orderedData.length);
            }
        });
    })
    .catch(function (error) {
        console.log('Error in GetAll from UserService');
        FlashService.Error(error);
    });
    };

    function movetoUpdate(username)  {
        console.log('in movetoUpdate function');
        $window.location.href="#/users/" + username;
        //$window.location.href="#/users/:" + user.username +"/:" + user.firstName +"/:" + user.lastName +"/:" + user.role;
   } 

   function cancel() {
        console.log('in cancel function');
        $window.location.href = "/app";
    }

    function deleteuser(index, deluser)
    {
        console.log('in deleteuser function of controller');
         if (deluser._id == $rootScope.user._id)
         {
            $window.alert('You cannot delete your own account!!!');
            return;
         }
         var r = confirm("Are you sure to delete user : " + deluser.firstName +' '+deluser.lastName + '?');
         if (r == true) {
              console.log('pressed OK to delete user ' + deluser.firstName +' '+deluser.lastName);
                      
              UserService.Delete(deluser._id)
                  .then(function () {
                      console.log('in deleteUser '+ index);
                      //$window.location = '/login';
                      var idx = tableData.indexOf(deluser);
                      tableData.splice( idx, 1 );
                      admin.tableParams.reload();

                  })
                  .catch(function (error) {
                      $window.alert(error);
                  });

          } else {
              console.log('pressed CANCEL to delete user ' + deluser.firstName +' '+deluser.lastName);
          }
    }


};

})();
