(function () {
    'use strict';

    angular
        .module('trainingApp', ['ui.router', 'ui.bootstrap', 'ngTable', 'ngTableExport', 'ngMaterial','jkAngularRatingStars'])
        .config(config)
        .run(run);

    function config($stateProvider, $urlRouterProvider) {

        // default route
        $urlRouterProvider.otherwise("/");
       

        $stateProvider            
        .state('submitRetailInquiry', {
                url: '/submitRetailInquiry',
                templateUrl: 'inquiry/submitretailinquiry.html',
                controller: 'inqsubmit.IndexController',
                controllerAs: 'inq',
                data: { 
                    activeTab: 'submitRetailInquiry',
                    //Array with permission state for Admin, Coun, Guest, Stu, Tra
                    stateAcc: [true, true, true, false, false]                    
                 }
            })
       .state('ViewAllInquiries', {
                url: '/ViewAllInquiries',
                templateUrl: 'inquiry/viewinquiry.html',
                controller: 'inqviewall.IndexController',
                controllerAs: 'inq',
                data: { 
                    activeTab: 'ViewAllInquiries',
                    stateAcc: [true, true, true, false, true] 
                }
            })
            .state('updateinquiry', {
                url: '/updateinquiry/{id}',
                templateUrl: 'inquiry/updateinquiry.html',
                controller: 'inquiryupdate.IndexController',
                controllerAs: 'inqu',
                data: { 
                    activeTab: 'ViewAllInquiries',
                    stateAcc: [true, true, true, false, false]
                     },
                resolve: {
                    inquirydata:['$stateParams','inquiryservice', function($stateParams, inquiryobj) {
                        console.log('inside resolve for updateinquiry ');
                        console.log($stateParams.id);
                        return inquiryobj.GetById($stateParams.id);
                    }]
                }
            })
          /*  .state('followup', {
                url: '/followup/{id}',
                templateUrl: 'inquiry/followup.html',
                controller: 'inquiryfollowup.IndexController',
                controllerAs: 'inqf',
                data: { activeTab: 'ViewAllInquiries',
                        stateAcc: [true, true, true, false, false]
                     },
                resolve: {
                    inquirydata:['$stateParams','inquiryservice', function($stateParams, inquiryobj1) {
                        console.log('inside resolve for followinquiry ');
                        console.log($stateParams.id);
                        return inquiryobj1.GetById($stateParams.id);
                    }]
                }
            })*/
       
       .state('EnrollStudent', {
                url: '/enrollStudent',
                templateUrl: 'student/createstudent.html',
                params: {
                    object: null
                },
                controller: 'createstudent.IndexController',
                controllerAs: 'stu',
                data: { 
                    activeTab: 'ViewAllInquiries',
                    stateAcc: [true, true, false, false, false]
                     }
            })
       .state('viewAllStudent', {
                url: '/viewAllStudent',
                templateUrl: 'student/viewallstudent.html',
                params: {
                    object: null
                },
                controller: 'viewallstudent.IndexController',
                controllerAs: 'stu',
                data: { activeTab: 'viewAllStudent',
                stateAcc: [true, true, false, false, true] }
            })
        
            .state('studentreview', {
                url: '/studentreview',
                templateUrl: 'student/studentreview.html',
                controller: 'studentreview.IndexController',
                controllerAs: 'strv',
                data: { activeTab: 'studentreview',
                        stateAcc: [true, true, false, true, false]
                     }
            })
            
       .state('viewStudent', {
                url: '/viewStudent',
                templateUrl: 'student/viewonestudent.html',
                controller: 'viewonestudent.IndexController',
                controllerAs: 'stu',
                data: { activeTab: 'viewOneStudent',
                        stateAcc: [true, true, false, false, false]
                }
            })
        .state('updateStudent', {
                url: '/updateStudent',
                templateUrl: 'student/updatestudent.html',
                params: {
                    object: null
                },
                controller: 'updatestudent.IndexController',
                controllerAs: 'stu',
                data: { activeTab: 'updateStudent',
                         stateAcc: [true, true, false, false, false]
                      }
            })

       .state('AddTrainer', {
                url: '/AddTrainer',
                templateUrl: 'trainer/addtrainer.html',
                controller: 'trainersubmit.IndexController',
                controllerAs: 'tra',
                data: { activeTab: 'AddTrainer',
                         stateAcc: [true, false, false, false, false]
                      }
            })
       .state('ViewAllTrainer', {
                url: '/ViewAllTrainer',
                templateUrl: 'trainer/viewtrainer.html',
                controller: 'traviewall.IndexController',
                controllerAs: 'tra',
                data: { activeTab: 'ViewAllTrainer',
                        stateAcc: [true, false, false, false, false]
                     }
            })
        .state('trainer', {
                url: '/trainer/{id}',
                templateUrl: 'trainer/updatetrainer.html',
                controller: 'trainerupdate.IndexController',
                controllerAs: 'tra',
                data: { activeTab: 'ViewAllTrainer',
                        stateAcc: [true, true, true, false, false] 
                    },
                resolve: {
                    trainerdata:['$stateParams','TrainerService', function($stateParams, trainer) {
            console.log('inside resolve ');
            console.log($stateParams.id);
            return trainer.getTrainer($stateParams.id);

                }]
            }
        })
    .state('createNewBatch', {
                url: '/createNewBatch',
                templateUrl: 'batch/createbatch.html',
                controller: 'createbatch.IndexController',
                controllerAs: 'bat',
                data: { 
                    activeTab: 'createNewBatch',
                    stateAcc: [true, false, false, false, false] 
                     }
            })
       .state('viewBatch', {
                url: '/viewBatch',
                templateUrl: 'batch/viewbatch.html',
                controller: 'viewbatch.IndexController',
                controllerAs: 'bat',
                data: { activeTab: 'viewBatch' }
            })
       .state('viewSpecificBatch', {
                url: '/viewSpecificBatch',
                templateUrl: 'batch/viewspecificbatch.html',
                params: {
                    object: null
                },
                controller: 'viewspecificbatch.IndexController',
                controllerAs: 'bat',
                data: { 
                    activeTab: 'viewSpecificBatch',
                    stateAcc: [true, true, true, false, false] 
                     }
            })
       .state('updateBatch', {
                url: '/updateBatch',
                templateUrl: 'batch/updatebatch.html',
                params: {
                    object: null
                },
                controller: 'updatebatch.IndexController',
                controllerAs: 'bat',
                data: { 
                    activeTab: 'viewSpecificBatch',
                    stateAcc: [true, false, false, false, false] 
                 }
            })
        .state('addNewCourse', {
                url:'/courses/create',
                templateUrl: 'course/addcourse.html',
                controller: 'addcourse.IndexController',
                controllerAs: 'cou',
                data: { 
                        activeTab: 'addNewCourse',
                        stateAcc: [true, false, false, false, false] 
                     } 
            })
       .state('getAllCourse', {
                url:'/courses/view',
                templateUrl: 'course/viewcourse.html',
                controller: 'viewcourse.IndexController',
                controllerAs: 'cou',
                data: { activeTab: 'getAllCourse',
                        stateAcc: [true, true, true, false, false]  } 
       })
       .state('course', {
                url: '/courses/{id}',
                templateUrl: 'course/updatecourse.html',
                controller: 'updatecourse.IndexController',
                controllerAs: 'cou',
                data: { 
                    activeTab: 'getAllCourse',
                    stateAcc: [true, false, false, false, false] 
                     },
                resolve: {
                    coursedata:['$stateParams','CourseService', function($stateParams, courseobj) {
                        console.log('inside resolve for updatecourse ');
                        console.log($stateParams.id);
                        var couin= courseobj.GetById($stateParams.id);
                        console.log(couin);
                        return couin;
                    }]
                }
        })
        .state('Admin', {
            url: '/Admin',
            templateUrl: 'Admin/Admin.html',
            controller: 'Admin.IndexController',
            controllerAs: 'Admin',
            data: { activeTab: 'Admin',
                    stateAcc: [true, false, false, false, false] 
                 }
        })
        .state('register', {
            url: '/users/register',
            templateUrl: 'administration/register.html',
            controller: 'register.IndexController',
            controllerAs: 'admin',
            data: { 
                    activeTab: 'register',
                    stateAcc: [true, false, false, false, false] 
                 }
        })
        .state('viewUser', {
            url: '/users/view',
            templateUrl: 'administration/viewuser.html',
            controller: 'viewuser.IndexController',
            controllerAs: 'admin',
            data: { activeTab: 'viewUser',
                    stateAcc: [true, false, false, false, false] 
                 }
        })
        .state('user', {
                url: '/users/{username}',
                templateUrl: 'administration/updateuser.html',
                controller: 'updateuser.IndexController',
                controllerAs: 'admin',
                data: { 
                        activeTab: 'viewUser',
                        stateAcc: [true, false, false, false, false] 
                     },
                resolve: {
                    userdata:['$stateParams','UserService', function($stateParams, userobj) {
                        console.log('inside resolve for updateuser ');
                        console.log($stateParams.username);
                        var couin= userobj.GetByUsername($stateParams.username);
                        console.log(couin);
                        return couin;
                    }]
                }
        })
        .state('account', {
                url: '/changepassword',
                templateUrl: 'account/account.html',
                controller: 'account.IndexController',
                controllerAs: 'acc',
                data: { activeTab: 'account' }
            })

        .state('Graph2', {
                url: '/Graph2',
                templateUrl: 'analytics/Graph2.html',
                controller: 'Graph2.IndexController',
                controllerAs: 'Gr2',
                data: { 
                        activeTab: 'Graph2',
                        stateAcc: [true, false, false, false, false] 
                     }
            });  
    }

    function run($http, $rootScope, $state, $window) {
        // add JWT token as default auth header
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + $window.jwtToken;

        $rootScope.$on('$stateChangeStart', function(e, to) {
        console.log('role idx '+$rootScope.user.roleIdx +' value' +to.data.stateAcc[$rootScope.user.roleIdx]);
        if (to.data.stateAcc && !to.data.stateAcc[$rootScope.user.roleIdx]) {
          e.preventDefault();
          $window.location='accessdenied.html';
        }
        });

        // update active tab on state change
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.activeTab = toState.data.activeTab;
        });
    }

    // manually bootstrap angular after the JWT token is retrieved from the server
    $(function () {
        // get JWT token from server
        $.get('/app/token', function (token) {
            window.jwtToken = token;

            angular.bootstrap(document, ['trainingApp']);
        });
    });
})();


