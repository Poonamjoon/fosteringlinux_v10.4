(function () {
    'use strict';

    angular
        .module('trainingApp')
        .factory('inquiryservice', Service);

    function Service($http, $q) {
        console.log('in App : inquiryservice');
        var service = {};

        service.getData = getData;
        service.createData = createData;
        service.GetById = GetById;
        service.Update = Update;
       // service.FollowUpdate = FollowUpdate;    //edited by poonam
        service.sendmail = sendmail; 
        service.StatusUpdate = StatusUpdate;
	service.getByEmail = getByEmail;  
        service.GetInqStatus = GetInqStatus;
        return service;

        function getData() {
            console.log('in inquiryservice : getData function');
            return $http.get('/app/inquiry').then(handleSuccess, handleError);
        }
    
        function createData(InquiryModel) {
            console.log('in inquiryservice : createData function');
            return $http.post('/app/inquiry',InquiryModel).then(handleSuccess, handleError);
        }
        
        
        function GetById(_id) {
            console.log('in inquiryservice : Get function for ' + _id);
            return $http.get('/app/inquiry/' + _id).then(handleSuccess, handleError);
        }


	function getByEmail(email) {
            console.log('in inquiryservice : Get function for email ' + email);
            return $http.get('/app/inquiry/',{params: {candmail:email}}).then(handleSuccess, handleError);            
        }
	

        //this update function called by updateinquiry.js file 
        //it will route to index.js file inquiry update function router.put('/inquiry/:_id',){}
        function Update(_id, inquiry) {
            console.log('in InquiryService : Update function');
            return $http.put('/app/inquiry/' +_id, inquiry).then(handleSuccess, handleError);
        }
	
      /*  function FollowUpdate(_id, inquiry) {
            console.log('in InquiryService : Follow Update function');
            return $http.put('/app/follow/' +_id, inquiry).then(handleSuccess, handleError);
        }*/
        function StatusUpdate(_id, inquirystat) {
            console.log('in InquiryService : Status Update function From Student Enroll');
            return $http.put('/app/status/' +_id, inquirystat).then(handleSuccess, handleError);
        }
        function GetInqStatus(_id) {
            console.log('in InquiryService : Get Status function From Followup');
            return $http.get('/app/inquiry/status' +_id).then(handleSuccess, handleError);
        }
        

        function sendmail(maildt) {
            console.log('in InquiryService :send mail function');
            return $http.post('/app/mail/' , maildt).then(handleSuccess, handleError);
            
        }
        // private functions

        function handleSuccess(res) {
            console.log(res.data);
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }
})();