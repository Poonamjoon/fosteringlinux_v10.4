/**
 * Created by tejas on 10-06-2016.
 */
angular.module('trainingApp')





    //fostering app inquery submitter
    .controller('inquiryupdate.IndexController', Controller)

    // Its user define directive service used for applying the filters into textboxes for enter only the numeric values 
    .directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });

function Controller($state,inquiryservice, CourseService,$scope,inquirydata) {



    $scope.inq = this;
     
     // object id is the userid which is default created by the mongodb it is coming from the mongodb
    $scope.inq.objectid = inquirydata._id ;
    // inq id is the random no. which is assigned according to the inquiry creation time which increments automatically according to inquiry increments
    $scope.inq.id = inquirydata.id;

    //open1,open2,open3,open4 is calander popups 
     $scope.inq.open1 = function() {
          $scope.inq.popup1.opened = true;
        };

        $scope.inq.popup1 = {
          opened: false
        };  

     $scope.inq.open2 = function() {
        $scope.inq.popup2.opened = true;
    };
    $scope.inq.popup2 = {
        opened: false
    };
   
    $scope.inq.open3 = function() {
        $scope.inq.popup3.opened = true;
    };
     $scope.inq.popup3 = {
        opened: false
    };
     $scope.inq.open4 = function() {
        $scope.inq.popup4.opened = true;
    };
     $scope.inq.popup4 = {
        opened: false
    };

      // to apply the readble date format & only include the date without time or day

     $scope.inq.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.inq.format = $scope.inq.formats[2];
    $scope.inq.altInputFormats = ['M!/d!/yyyy'];
     

    $scope.inq.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };





    $scope.inquirystatus = inquirydata.stat1;
    if($scope.inquirystatus=="Enroll"){
        var msg = "Inquiry For This Student Is Closed , It Cannot Be Updated"+"\n\n"+"\t"+"!!! You Are Being Redirect" +
            " From" +
            " This" +
            " Page !!! ";
        alert(msg);
        window.location = "/app/#/ViewAllInquiries";

    }


    if($scope.inquirystatus=="Reject"){
        var msg = "Inquiry For This Student Is Rejected , It Cannot Be Updated"+"\n\n"+"\t"+"!!! You Are Being" +
            " Redirect" +
            " From" +
            " This" +
            " Page !!!";
        alert(msg);
        window.location = "/app/#/ViewAllInquiries";


    }


    console.log(inquirydata);
$scope.inq.candname = inquirydata.candname;
    $scope.inq.candmail = inquirydata.candmail;
    $scope.inq.candnum1 = inquirydata.candnum1;
    $scope.inq.objectid = inquirydata._id ;
$scope.inq.id = inquirydata.id;
    $scope.inq.destrg=inquirydata.destrg;
    $scope.inq.startTime =inquirydata.startTime;
    $scope.inq.endTime = inquirydata.endTime;
    $scope.inq.desday=inquirydata.desday;
    $scope.inq.desdate=inquirydata.desdate;
    $scope.inq.desloc=inquirydata.desloc;
    $scope.inq.othCou=inquirydata.othCou;
    $scope.inq.othfee=inquirydata.othfee *1;

    $scope.inq.othdur=inquirydata.othdur;
    $scope.inq.fosc=inquirydata.fosc;
    $scope.inq.dosc=inquirydata.dosc;
//    $scope.inq.difee = inq.difee;
    $scope.inq.stax=inquirydata.stax;
    $scope.inq.dio = inquirydata.dio;
    $scope.inq.da=inquirydata.da;
    $scope.inq.netfee=inquirydata.netfee;

    $scope.inq.sumofdis = inquirydata.sumofdis;

    $scope.inq.handler = inquirydata.handler;

     $scope.inq.followdate1=inquirydata.followdate1;
    $scope.inq.followdate2=inquirydata.followdate2;
    $scope.inq.followdate3=inquirydata.followdate3;

  //  console.log(inqupdate.handler);
   $scope.inq.courseList=[];
$scope.fl = inquirydata.handler;

//Get Course List from Course Service

  
    CourseService.GetAll()
        .then(function (response) {
            console.log('Success in Course GetAll');
            $scope.inq.courseList=response;


        })
        .catch(function (error) {
            FlashService.Error(error);
        });


// get fee from course list
    $scope.getfee = function (item) {
        if (item=="Others") {
            $scope.inq.fosc = 0;
            $scope.inq.dosc = 0;
            $scope.inq.destrgold = "Others";

        }
        else{
$scope.inq.destrgold = $scope.inq.destrg;
            $scope.inq.othCou = "";
            $scope.inq.othfee = 0;
            $scope.inq.othdur = "";
            var result = [];
            var searchfield = "name";
            var searchval = item;
            for (i = 0; i < $scope.inq.courseList.length; i++) {

                if ($scope.inq.courseList[i][searchfield] == searchval) {
                    $scope.inq.fosc = $scope.inq.courseList[i].fee;
                    console.log($scope.inq.courseList[i].fee);
                    $scope.inq.dosc = $scope.inq.courseList[i].duration;
                    console.log($scope.inq.courseList[i].duration);
                    $scope.inq.stax = $scope.inq.courseList[i].srvctax;

                    $scope.inq.netfee =  $scope.inq.fosc + (($scope.inq.fosc)*$scope.inq.stax/100);
                }
            }

        }
    }

    $scope.nefee = function () {

        var afee ="";
        var pfee = $scope.inq.fosc *1 ;
        var d = $scope.inq.dio *1;
        var st = $scope.inq.stax *1;
        
        if($scope.inq.dio==0){

            $scope.inq.difee = (pfee * (1-(d/100)) );

            $scope.inq.netfee = ($scope.inq.difee*(1+(st/100)));


        }

        else{
           
            //calculating actual fee as afee
            $scope.inq.difee = (pfee * (1-(d/100)) );
            $scope.inq.netfee = ($scope.inq.difee*(1+(st/100)));




        }

    }

    // if student is not enroll then enroll using this function
    $scope.inq.createStudent = function () {

    console.log('Netfee :: '+inquirydata.netfee);
    var obj = {inqid: inquirydata._id, 
        stuname: inquirydata.candname, 
        stumail: inquirydata.candmail, 
        stunum:inquirydata.candnum1, 
        trgloc: inquirydata.desloc, 
        trgcou: inquirydata.destrg, 
        suggfee: inquirydata.netfee,
        status:inquirydata.stat1};
    if(inquirydata.stat1=="Enroll")
        {alert("!!! Student Is Already Enrolled !!!");
        return;
    }
    $state.go("EnrollStudent", {object:obj});
}
    
    // to update inquiry in db this function is called by the inquiryservice.js
    
    $scope.inq.updateinquiry = function () {


        var cdeml = document.getElementById("candmail").value;
        if (cdeml=="") {
            alert("!!!! Candidate Email Cannot Be Blank !!!");


            return;
        }
        var atpos = cdeml.indexOf("@");
        var dotpos = cdeml.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=cdeml.length) {
            alert("!!!! Candidate Email Is Not a valid e-mail address !!!!");

            return;
        }

        var cdeml = document.getElementById("candnum1").value;
        if (cdeml=="") {
            alert("!!!! Candidate Contact Number Cannot Be Blank !!!");


            return;
        }


        var ditc = document.getElementById("inqdestrg").value;

        if (ditc=="? undefined:undefined ?") {
            alert("!!!! Desired Training Course In Training Details cannot be blank !!!");


            return;
        }

        var othecrse = document.getElementById("othcrse").value;
        if (ditc=="Others" && othecrse==""   ) {
            alert("!!!! New Training Course In Training Details cannot be blank !!!");


            return;
        }

        var tdio = document.getElementById("dio").value;
        var tda = document.getElementById("da").value;
        if (tdio>0 && tda=="" && ditc!="Others" ) {

            alert("!!!! Person Name For Discount Approved In Training Details Cannot be blank !!!");

            return;
        }
        if (tdio=="" && ditc!="Others") {

            alert("!!!! Discount Offered In Training Details Cannot Be Blank !!!");


            return;
        }

         //added by poonam
       var fl1 = document.getElementById("followdate1").value;
        if (fl1=="" && $scope.inq.followdate1==null) {
            alert("!!!! Follow Up 1 Date cannot be blank !!!");

            return; }

        var re1 = document.getElementById("rem1").value;
        if (re1=="") {
            alert("!!!! Remarks 1 cannot be blank !!!");

            return; }


        var st1=document.getElementById("stat1").value;
        if(st1=="")
        {
          alert("!!!! Please select Inquiry Status !!!");  
          return;
        }
        var dbInqStaus =  null;
        var set = null;

         if($scope.inq.stat1=="Enroll")
        { 
            //fetch inquiry status from DB and check whether Student is enrolled or not.
            inquiryservice.GetInqStatus(inquirydata._id)
            .then(function (response) {
                console.log('Success in GetInqStatus');
                dbInqStaus=response;                
            })
            .catch(function (error) {
               console.log(error);
            });

            if (dbInqStaus!="Enroll")
            {
                alert("Before submitting Follow-up details, Please Enroll Student by clicking on Enroll button.");
                return;
            }
        }

        // if status is enroll then It will set the candidate details as they are  other wise goto the else case
        // set is a mongodb function to reset the values into mongodb document

        if (dbInqStaus=="Enroll")
        {
            //No need to update Status as it is already updated through Create Student
            set  = {
                id:$scope.inq.id,
                 candname:$scope.inq.candname,
            candmail:$scope.inq.candmail,
            candnum1:$scope.inq.candnum1,
            destrg:$scope.inq.destrg,

                startTime:$scope.inq.startTime,
                endTime:$scope.inq.endTime,
                desday:$scope.inq.desday,
                desmon:$scope.inq.desdate,
                desloc:$scope.inq.desloc,
                othCou:$scope.inq.othCou,
                othfee:$scope.inq.othfee,
                othdur:$scope.inq.othdur,
                fosc:$scope.inq.fosc,
                dosc:$scope.inq.dosc,
                stax:$scope.inq.stax,
                dio:$scope.inq.dio,
                difee:$scope.inq.difee,
                da:$scope.inq.da,
            netfee:$scope.inq.netfee,
            sumofdis:$scope.inq.sumofdis,
                followdate1: $scope.inq.followdate1,
                rem1: $scope.inq.rem1,             
                followdate2: $scope.inq.followdate2,
                rem2: $scope.inq.rem2,   
                followdate3: $scope.inq.followdate3,
                rem3: $scope.inq. rem3
                };
        }
        else                     // if status is other than enroll then it will update the details after getting the  
        {                        // form data 
            set  = {
                  id:$scope.inq.id,
            candname:$scope.inq.candname,
            candmail:$scope.inq.candmail,
            candnum1:$scope.inq.candnum1,
            destrg:$scope.inq.destrg,

                startTime:$scope.inq.startTime,
                endTime:$scope.inq.endTime,
                desday:$scope.inq.desday,
                desmon:$scope.inq.desdate,
                desloc:$scope.inq.desloc,
                othCou:$scope.inq.othCou,
                othfee:$scope.inq.othfee,
                othdur:$scope.inq.othdur,
                fosc:$scope.inq.fosc,
                dosc:$scope.inq.dosc,
                stax:$scope.inq.stax,
                dio:$scope.inq.dio,
                difee:$scope.inq.difee,
                da:$scope.inq.da,
            netfee:$scope.inq.netfee,
            sumofdis:$scope.inq.sumofdis,
                followdate1: $scope.inq.followdate1,
                rem1: $scope.inq.rem1,
                stat1: $scope.inq.stat1,
                followdate2: $scope.inq.followdate2,
                rem2: $scope.inq.rem2,
               
                followdate3: $scope.inq.followdate3,
                rem3: $scope.inq. rem3
               
            };   
        }
       
                // Inquiry service will call the objectid (same as created at the time of inquiry) 
                // and set object from the above code 
        console.log('in updateinquiry:updateinquiry function');

        // update function will call the inquiry.service.js file  Update() function 

        inquiryservice.Update ( $scope.inq.objectid,set)
            .then(function () {
               alert('Inquiry updated successfully!!!');
                var moveToAddCou = false;
                if ($scope.inq.destrg == 'Others')
                {
                    var r = confirm("You have selected course as Others. Do you like to add course :"+$scope.inq.othCou+"?");
                    if (r == true) {
                        console.log('pressed OK to add new course ' + $scope.inq.othCou);
                        moveToAddCou = true;
                    } else {
                        console.log('pressed CANCEL to ignore Other course' + $scope.inq.othCou);
                    }
                }

                if (moveToAddCou)
                {
                    $state.go("addNewCourse");
                }
                alert("!!!You are being redirected to View Inquiry Page!!!");
                window.location = "/app/#/ViewAllInquiries";

                
            })
            .catch(function (error) {
                console.log('Error from db ' + error);

                console.log(error.code);
                if (error.code == 11000)
                {
                    var field = error.message.split('.$')[1];
                    field = field.split(' dup key')[0];
                    field = field.substring(0, field.lastIndexOf('_'));

                    console.log('field : ' + field);
                    var msg;
                    if(field=="id")
                        msg="Failed to Update Inquiry. Duplicate Inquiry ID found !";
                    else
                        msg="Failed to Update Inquiry. Duplicate Inquiry Name found !";

                     alert(msg);

                }
                else
                {
                    console.log(error);
                   alert(error);
                }

            });

    };









    $scope.inq.getInquiry=function()
    {
        console.log('in getInquiry func')
        return inquiryservice.Get[$stateParams.id];
    };


   

    $scope.inq.cancel = function () {

        alert("Redirecting Back to Dashboard");



    }









}

